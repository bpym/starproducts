.. Poisson Brackets documentation master file, created by
   sphinx-quickstart2 on Tue Aug 30 10:44:03 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Poisson Brackets's documentation!
============================================

Contents:

.. toctree::
	:maxdepth: 2

	poisson
	star
	kontsevich
	kontsevich_generators
	point_scheme

.. [Kontsevich2003] \M. Kontsevich, Deformation quantization of {P}oisson manifolds.
   Lett. Math. Phys. 2003

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

