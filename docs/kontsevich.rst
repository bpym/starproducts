Kontsevich Graphs
=================
.. automodule:: graphs.kontsevichgraph

	.. autoclass:: KontsevichAdmissibleGraph
		:members:

	.. autoclass:: KontsevichFormalityGraph
		:members:

	.. autoclass:: KontsevichQuantizationGraph
		:members:

