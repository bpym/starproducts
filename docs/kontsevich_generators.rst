Graph Generators
================
.. automodule:: graphs.kontsevichgraph_generators
	:members: get_iso_classes

	Kontsevich Graph Generator Class
	--------------------------------
	.. autoclass:: KontsevichGraphGenerators
		:members:
