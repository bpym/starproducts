Poisson Algebra
===============
.. automodule:: poisson_brackets.poisson_bracket
	:members: jacobiator

	Poisson Bracket class
	---------------------
	.. autoclass:: PoissonBracket
		:members:
