# StarProducts

This git repository hosts code for calculations with Poisson brackets and Kontsevich's star product.  For guidance on using the code, please see [the tutorial](src/tutorial.ipynb) (a Jupyter notebook), which you can open by running `sage -n jupyter tutorial.ipynb` in the `src` directory.

Please note that the current version of the code should be consider a working proptotype, rather than a stable release.  In particular, we are aware of some bugs, and we may eventually rebuild some parts of the code using SageManifolds, which was included in SageMath after the bulk of our code was written.  Please contact Brent Pym (`brent.pym@ed.ac.uk`) if you are interested in contributing to development.

Primary developers:

* Peter Banks

Other developers:

* Brent Pym
* Kelvin Ritland

## Weight calculation

The program to calculate the weights of formality graphs is developed by Erik Panzer and provided in the [kontsevint repository](https://bitbucket.org/PanzerErik/kontsevint/src/master/). Please look there for details on the integration algorithm, lists of calculated weights and updates.

To invoke this program and calculate further weights from within the Sage package, it is necessary to [install the `kontsevint` module](src/ext/maple/kontsevichgraph_weight/README.md).


## Installation

Once the code has been downloaded, it can be loaded into Sage by running Sage in the `src` directory of the package, and then running `from all import *`. This will load all of the relevant classes from the library.

If you want the package to be loaded automatically on startup, you can add the following to `~/.sage/init.sage`:

~~~~
starproducts_path = '/path/to/StarProducts'

import imp
import os
sys.path.append(starproducts_path)

starproducts = imp.load_source('starproducts', os.path.join(starproducts_path, 'src', 'all.py'))
from starproducts import *
~~~~

## Running doctests

Use the following command from the `src` directory to run all doctests:
~~~~
starproducts/src$ PATH=$PATH:. PYTHONPATH="." sage -t --long --environment all .
~~~~

The `PATH` modification is needed in order to invoke the nauty commands (geng,
directg, pickg) included in the src directory.  The `--environment all` and
`PYTHONPATH` flags are needed to load the relevant starproduct modules before
running the tests.

To skip long running tests, omit the `--long` flag:
~~~~
starproducts/src$ PATH=$PATH:. PYTHONPATH="." sage -t -environment all .
~~~~
