r"""Examples of Poisson Brackets

TODO: add docstrings to these functions
"""

from sage.calculus.var import function
from sage.matrix.constructor import Matrix
from sage.rings.rational import Rational
from sage.rings.rational_field import QQ
from sage.symbolic.ring import SR

from .poisson_bracket import PoissonBracket

COORDINATE_NAMES = ['x', 'y', 'z', 'v', 'w']
COORDINATE_FORMAT = 'x_{}'

PARAMETER_NAMES = ['p', 'q', 'r', 's', 't', 'u']
PARAMETER_FORMAT = 't_{}'

def names(fixed, name_format, n, shift=1):
    if n <= len(fixed):
        return fixed[:int(n)]
    return [name_format.format(i) for i in range(shift, n + shift)]


def setup_variables(vars, params, vars_order, params_order=None):
    if params_order is None:
        params_order = vars_order

    if vars is None and vars_order > 0:
        vars = SR.var(','.join(names(COORDINATE_NAMES, COORDINATE_FORMAT, vars_order)))

    if params is None and params_order > 0:
        if params_order == 1:
            params = SR.var('t')
        else:
            params = SR.var(','.join(names(PARAMETER_NAMES, PARAMETER_FORMAT, params_order)))

    return vars, params

def AccyBracket(vars=None):
    vars = setup_variables(vars, None, 2, 0)[0]
    a = SR.var('a')
    order = len(vars)
    R = PoissonBracket.make_ring(QQ, *[a])
    delta = lambda i: vars[i-1] if i>0 else 0
    gamma = lambda i: (a + i)*vars[i]
    entry = lambda i, j: delta(i)*gamma(j) - delta(j)*gamma(i)

    data = [[entry(i, j) for j in range(order)] for i in range(order)]

    return PoissonBracket(vars, data, R, format='matrix', make_skew=False)


def DarbouxBracket(vars=None):
    vars = setup_variables(vars, None, 2, 0)[0]

    return PoissonBracket(vars, [[0, 1], [-1, 0]])


def SkewBracket(vars=None, t=None):
    return ToricBracket(vars, t, 2)


def JordanBracket(vars=None, order=2):
    vars = setup_variables(vars, None, 2, 0)[0]

    return PoissonBracket(vars, [[0, vars[0]**order], [-vars[0]**order, 0]])

def ToricBracket(vars=None, params=None, order=3):
    vars, params = setup_variables(vars, params, order, (order * (order - 1)) / 2)

    if order == 2:
        # We only have one parameter, so make it into a list of length 1
        params = [params]

    half = Rational((1, 2))
    R = PoissonBracket.make_ring(QQ, *params)

    entry = lambda i, j: half * (-1)**(i + j + 1) * vars[i] * vars[j] if i < j else 0

    data = [[entry(i, j) for j in range(order)] for i in range(order)]
    k = 0
    for i in range(order):
        for j in range(i + 1, order):
            data[i][j] *= params[k]
            k += 1

    return PoissonBracket(vars, data, R, format='matrix', make_skew=True)


def SklyaninBracket(vars=None, t=None):
    vars, t = setup_variables(vars, t, 3, 1)

    f = Rational((1, 3)) * (vars[0]**3 + vars[1]**3 + vars[2]**3) + t * vars[0] * vars[1] * vars[2]
    R = PoissonBracket.make_ring(QQ, t)

    return PoissonBracket(vars, [f], R, format='jacobian')


def GenericBracket(order, vars=None):
    vars = setup_variables(vars, None, order, 0)[0]

    if order == 1:
        return PoissonBracket([vars], [[0]], SR, format='matrix')

    if len(vars) != order:
        raise ValueError("Incorrect number of variables (need {}, got {})".format(order, len(vars)))

    poisson_matrix = Matrix(SR, nrows=order, ncols=order)
    for i in range(order):
        for j in range(i + 1, order):
            poisson_matrix[i, j] = function('pi_{}{}'.format(i, j))(*vars)

    return PoissonBracket(vars, poisson_matrix, SR, format='matrix', make_skew=True, ignore_jacobi=True)

def Log112():
    x0, x1, x2, x3 = SR.var('x0,x1,x2,x3')
    c0, c1, t = SR.var('c0,c1,t')
    poisson_matrix = Matrix([[0, 0, c0*x0*x2, -c0*x0*x3], [0, 0, -c1*x1*x2, c1*x1*x3], [0, 0, 0, (c0-c1)*(x0**2+t*x0*x1+x1**2+x2*x3)], [0, 0, 0, 0]])
    return PoissonBracket([x0, x1, x2, x3], poisson_matrix, SR, format='matrix', make_skew=True)

dictionary = {
    'darboux': DarbouxBracket,
    'skew': SkewBracket,
    'jordan': JordanBracket,
    'sklyanin': SklyaninBracket,
    'toric': ToricBracket
}
