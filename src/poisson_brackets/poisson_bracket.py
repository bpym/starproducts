r"""Allow the definition and use of Poisson brackets.

AUTHORS:

- Peter Banks (2016-08-27): initial version

EXAMPLES::

    sage: P = poisson_brackets.SkewBracket()
    sage: P.poisson_matrix
    [           0    1/2*t*x*y]
    [(-1/2*t)*x*y            0]
    sage: x, y = P.vars
    sage: P.bracket(x, y)
    1/2*t*x*y
"""

from copy import copy
from functools import partial

from sage.structure.sage_object import SageObject
try:
    from sage.algebras.lie_algebras.lie_algebra import LieAlgebra
except ImportError:
    # LieAlgebras were not introduced as part of Sage 7.
    #
    # Before using a LieAlgebra object, we check to see if an object is an
    # instance of the LieAlgebra class. By defining this dummy class here,
    # the check will always fail.
    class LieAlgebra(SageObject):
        pass

from sage.calculus.functional import derivative
from sage.calculus.functions import jacobian
from sage.combinat.combination import Combinations
from sage.matrix.constructor import Matrix
from sage.modules.free_module_element import vector
from sage.rings.ideal import Ideal
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.rings.rational_field import QQ
from sage.structure.element import is_Matrix
from sage.symbolic.operators import add_vararg
from sage.symbolic.ring import SR

def jacobiator(f):
    """Create the Jacobiator for f.

    INPUT:

    - ``f`` -- the two-variable function we want to calculate the Jacobiator of

    OUTPUT:

    The function $J(x,y,z) := f(x, f(y, z)) + f(y, f(z, x)) + f(z, f(x, y))$
    as a lambda function.

    EXAMPLES:
    ::

        sage: from poisson_brackets.poisson_bracket import jacobiator
        sage: J = jacobiator(operator.mul)
        sage: var('x,y,z')
        (x, y, z)
        sage: J(x, y, z)
        3*x*y*z

    The function f must be callable with two arguments:

    ::

        sage: from poisson_brackets.poisson_bracket import jacobiator
        sage: J = jacobiator(abs)
        sage: var('x,y,z')
        (x, y, z)
        sage: J(x, y, z)
        Traceback (most recent call last):
        ...
        TypeError: abs() takes exactly one argument (2 given)
    """
    return lambda x, y, z: f(x, f(y, z)) + f(y, f(z, x)) + f(z, f(x, y))

class DerivativeCache(SageObject):
    """Provide a way of storing a list of coordinate variables and calculating cached derivatives.

    This class is used to allow the user to easily take the derivative of a
    function with respect to the $i^{th}$ coordinate.

    We cache the results of differentiation to speed up computation time.

    EXAMPLES:
    ::

        sage: from poisson_brackets.poisson_bracket import DerivativeCache
        sage: var('x,y,z')
        (x, y, z)
        sage: D = DerivativeCache([x, y, z])
        sage: D.derivative(x**3, 0, 0)
        6*x
        sage: D.derivative(x*y, 0, 1)
        1
        sage: D.derivative(x*y, 2)
        0

    .. SEEALSO::

        :meth:`DerivativeCache.derivative`.
    """

    def __init__(self, coord_vars):
        """Create a new derivative cache.

        INPUT:

        - ``coord_vars`` - a list of coordinate variables to use for this object

        OUTPUT:

        A new cache object.

        EXAMPLES:
        ::

            sage: from poisson_brackets.poisson_bracket import DerivativeCache
            sage: var('x, y')
            (x, y)
            sage: D = DerivativeCache([x,y])

        The parameter ``coord_vars`` must be iterable:
        ::

            sage: from poisson_brackets.poisson_bracket import DerivativeCache
            sage: D = DerivativeCache(x)
            Traceback (most recent call last):
            ...
            TypeError: 'sage.symbolic.expression.Expression' object is not iterable
        """
        self.vars = tuple(coord_vars)
        self.cache = {}
        self.matrix_cache = {}

    def _latex_(self):
        r"""Return the LaTeX representation of this object.

        EXAMPLES::

            sage: from poisson_brackets.poisson_bracket import DerivativeCache
            sage: var('x, y, z')
            (x, y, z)
            sage: D = DerivativeCache([x, y, z])
            sage: latex(D) # indirect doctests
            \text{Derivative cache on }(x, y, z)
        """
        return "\\text{{Derivative cache on }}{}".format(self.vars)

    def _repr_(self):
        r"""Return a string representing this object.

        EXAMPLES::

            sage: from poisson_brackets.poisson_bracket import DerivativeCache
            sage: var('x, y, z')
            (x, y, z)
            sage: D = DerivativeCache([x, y, z])
            sage: repr(D) # indirect doctest
            'Derivative cache on (x, y, z)'
        """
        return "Derivative cache on {}".format(self.vars)

    def dimension(self):
        r"""Return the number of variables we know about.

        INPUT:
        None

        OUTPUT:

        The dimension of this cache.

        EXAMPLES::

            sage: from poisson_brackets.poisson_bracket import DerivativeCache
            sage: x,y,z = var('x,y,z')
            sage: cache = DerivativeCache([x,y])
            sage: cache.dimension()
            2
            sage: cache = DerivativeCache([x,y,z])
            sage: cache.dimension()
            3
        """
        return len(self.vars)

    def derivative_index(self, matrix, indices, derivatives):
        r"""Calculate the derivative of an element of a matrix with respect to a list of variables.

        INPUT:

        - ``matrix`` -- the matrix which we are using
        - ``indices`` -- the indices used to index ``matrix`` as a list
        - ``derivatives`` -- a list of integers, with each integer specifying a derivative to take

        OUTPUT:

        The result of applying the list derivatives given to the element specified in the matrix.

        EXAMPLES:
        ::

            sage: from poisson_brackets.poisson_bracket import DerivativeCache
            sage: vars = var('x,y,z')
            sage: cache = DerivativeCache([x,y,z])
            sage: cache.derivative_index([[0, x**2],[-x**2,0]], [1,0], [0])
            -2*x
            sage: cache.derivative_index([[0, x**2],[-x**2,0]], [1,0], [1])
            0

        ::

            sage: from poisson_brackets.poisson_bracket import DerivativeCache
            sage: D = DerivativeCache([x])
            sage: D.derivative_index([], [1,1], [0])
            Traceback (most recent call last):
            ...
            IndexError: list index out of range

        .. SEEALSO::

            :meth:`derivative`
        """
        for i in indices:
            matrix = matrix[i]

        return self.derivative(matrix, derivatives)

    def derivative(self, f, *args):
        r"""Calculate a derivative, and cache the result

        INPUT:

        - ``f`` -- the expression to differentiate
        - ``*args`` -- a list of integers, with each integer specifying a derivative to take

        OUTPUT:

        The derivative of ``f`` with respect to each variable index passed.

        EXAMPLES:
        ::

            sage: from poisson_brackets.poisson_bracket import DerivativeCache
            sage: D = DerivativeCache(var('x,y,z'))
            sage: D.derivative(x*y, 0, 1)
            1
            sage: D.derivative(x**5 + x*y**4, 0)
            5*x^4 + y^4

        We can differentiate symbolic functions
        ::

            sage: from poisson_brackets.poisson_bracket import DerivativeCache
            sage: D = DerivativeCache(var('x,y'))
            sage: D.derivative(sin(x), 0)
            cos(x)

        It is possible to differentiate with respect to a list of variables
        ::

            sage: from poisson_brackets.poisson_bracket import DerivativeCache
            sage: D = DerivativeCache(var('x,y, z'))
            sage: f = function("f")(x,y,z)
            sage: D.derivative(f, [0, 1, 2])
            diff(f(x, y, z), x, y, z)
            sage: g = exp(y/z)
            sage: D.derivative(g, 0, 1)
            0
            sage: D.derivative(g, 1, 2)
            -y*e^(y/z)/z^3 - e^(y/z)/z^2

        It is an error for the derivatives to be the coordinates themselves
        ::

            sage: from poisson_brackets.poisson_bracket import DerivativeCache
            sage: D = DerivativeCache([var('x')])
            sage: D.derivative(x**2, x)
            Traceback (most recent call last):
            ...
            TypeError: unable to convert x to an integer

        It is an error for the derivatives to be non-integers, or not in the
        range ``[0, dimension())``
        ::

            sage: from poisson_brackets.poisson_bracket import DerivativeCache
            sage: D = DerivativeCache((x,))
            sage: D.derivative(x**5, 1)
            Traceback (most recent call last):
            ...
            IndexError: tuple index out of range
            sage: D.derivative(x**5, 1.5)
            Traceback (most recent call last):
            ...
            TypeError: tuple indices must be integers or slices, not sage.rings.real_mpfr.RealLiteral

        ALGORITHM:

        We check to see whether or not this function and set of derivatives has
        already been calculated. If it has, we return that. Otherwise, we try
        to split the expression up into a sum, and then call this procedure
        recursively. If this expression is not a sum, then we differentiate the
        expression, using either f.derivative(...) if available or
        derivative(f, ...) if not.
        """

        if not args:
            return f

        if not isinstance(args[0], list):
            return self.derivative(f, list(args))

        if not args[0]:
            return f

        derivatives = sorted(args[0])

        key = (f, tuple(derivatives))

        try:
            # If this is already in the cache, use that
            result = self.cache[key]
        except KeyError:
            if hasattr(f, 'operator') and f.operator() == add_vararg:
                # If this is a sum, split it up
                result = add_vararg(*[self.derivative(term, derivatives) for term in f.operands()])
            else:
                # Apply the first derivative and see what's left
                if hasattr(f, 'derivative'):
                    result = f.derivative(self.vars[derivatives[0]])
                else:
                    result = derivative(f, self.vars[derivatives[0]])

                if len(derivatives) > 1:
                    result = self.derivative(result, derivatives[1:])

                self.cache[key] = result

        return result

    def jacobiator(self, f):
        r"""Return the Jacobiator in terms of coordinate variables.

        INPUT:

        - ``f`` -- the expression to calculate the Jacobiator of

        OUTPUT:

        The Jacobiator which expects arguments as integers, each integer
        being an index into a coordinate variable of this class.

        EXAMPLES::

            sage: from poisson_brackets.poisson_bracket import DerivativeCache
            sage: import operator
            sage: D = DerivativeCache(var('x,y,z'))
            sage: J = D.jacobiator(operator.mul)
            sage: J(0, 1, 2)
            3*x*y*z

        .. SEEALSO::

            :meth:`jacobiator`
        """

        J = jacobiator(f)
        return lambda i, j, k: J(self.vars[i], self.vars[j], self.vars[k])


class PoissonBracket(DerivativeCache):
    r"""A class allowing the definition and computation of Poisson brackets.

    A Poisson bracket is a binary operator $\{\cdot, \cdot\}$ which satisfies
    the following properties:

    - Anticommutativity ($\{f, g\} = -\{g, f\}$)
    - Linearity ($\{\lambda*f_1 + f_2, g\} = \lambda*\{f_1, g\} + \{f_2, g\}$)
    - Product rule ($\{f, g_1g_2\} = \{f, g_1\} + \{f, g_2\}$)
    - The Jacobi identity ($\{f_1, \{f_2, f_3\}\} + \text{cyclic permuations}
      = 0$)

        This classes implements a Poisson bracket over an affine space of
        arbitrary dimension. Let the coordinates be $x_1, \ldots, x_n$; then one
        can write the Poisson bracket as a bivector

    \[ \pi = \sum \pi_{i,j} \partial_{x_i} \wedge \partial_{x_j} \]

    Using this representation, we store the coefficients of the tensor as a
    matrix. We also store a list of the coordinate variables of the manifold,
    allowing us to take derivatives with respect to the ith variable.

    .. SEEALSO::

        :wikipedia:`Poisson_manifold`
    """

    def __init__(self, vars, data=None, base_ring=QQ, format='detect', ignore_jacobi=False, make_skew=False):
        r"""Create a Poisson bracket with tensor described by data.

        INPUT:

        - ``vars`` -- a list of coordinate variables
        - ``data`` -- depending on the value of ``format``, either a matrix or
          a list of functions.
        - ``base_ring`` -- (default \\QQ) the ring over which this manifold is
          defined
        - ``format`` -- (default 'detect') the format for data; one of
          'detect', 'matrix' or 'jacobian'
        - ``ignore_jacobi`` -- whether to skip the check of the Jacobi identity
          for this bracket (e.g. if the matrix is a list of symbolic functions)
        - ``make_skew`` -- if True and ``format`` is set to ``matrix``, use the
          upper-right triangle of the matrix to make the lower-left triangle of
          the matrix skew-symmetric.

        OUTPUT:

        A Poisson bracket object.

        A Poisson bracket can be constructed from a number of different inputs.
        If ``vars`` is a Lie algebra, then the Poisson bracket constructed
        corresponds to the Poisson structure on the tensor algebra of the Lie
        algebra.

        Otherwise, we take vars as a list of coordinate variables for the
        manifold.

        With ``format`` set to 'matrix', we interprate data as a nxn matrix,
        where n=len(vars), and create a Poisson tensor with $\{x_i, x_j\} =
        \pi_{i,j} = data[i,j]$. If ``make_skew`` evaluates to True, we use the
        upper right triangle of the matrix to initialise the lower left
        triangle.

        With ``format`` set to 'jacobian', we construct a Jacobian Poisson
        structure by contracting the (n-2)-form $df_1 \wedge \ldots \wedge
        df_n$ into $\del_1 \wedge \ldots \wedge \del_n$, where $f_i = data[i]$
        is a function depending upon ``vars``.

        If format='detect', we try and guess which method we want.

        We expect all of the entries in the matrix to lie in the PolynomialRing
        ``base_ring`` extended by each of the variables. Thus if symbols are to
        be used, ``base_ring`` should be changed accordingly (to either a ring
        containing these symbols or the entire symbolic ring).

        This class normally checks that the Jacobi identity is satisfied by
        its tensor. This behvaiour can be overidden by passing ignore_jacobi=
        True. This can be useful when the user knows that the Jacobi identity
        will be satisfied but the computer won't be able to detect this e.g if
        we have a list of arbitrary symbolic functions about whic Sage knows
        nothing.

        ALGORITHM:

        The format auto-detection is based on the length of the data provided.
        If it is a matrix, or of length n = len(vars), it is taken to be a matrix
        giving the Poisson bivector. If it is of length n-2, it is taken to be a
        list of functions to calculate the Jacobian of.

        This function also calls meth:`validate` to check the given data is a
        valid Poisson bracket.

        NOTES:
        We normally convert each element of the matrix to the PolynomialRing
        ``base_ring`` extended by each of the coordinates. However, if
        ``base_ring`` is the symbolic ring, then the matrix is over the
        symbolic ring as well.

        EXAMPLES:
        Construct a constant Poisson bracket.
        ::
            sage: var('x,y')
            (x, y)
            sage: P = PoissonBracket([x,y], [[0, 1], [-1, 0]])
            sage: P.poisson_matrix
            [ 0  1]
            [-1  0]

        Construct the Poisson bracket corresponding to the Lie algebra SL(2).
        ::

            sage: P = PoissonBracket(lie_algebras.sl(SR, 2))
            sage: P.poisson_matrix
            [      0      h1 (-2)*e1]
            [    -h1       0    2*f1]
            [   2*e1 (-2)*f1       0]

        Construct a Jacobian Poisson bracket.
        ::
            sage: var('x,y,z')
            (x, y, z)
            sage: P = PoissonBracket([x,y,z], [x**3 + y**3 + z**3 + 3*x*y*z])
            sage: P.poisson_matrix
            [             0  3*x*y + 3*z^2 -3*y^2 - 3*x*z]
            [-3*x*y - 3*z^2              0  3*x^2 + 3*y*z]
            [ 3*y^2 + 3*x*z -3*x^2 - 3*y*z              0]

        Make a Poisson bracket from only half a matrix.
        ::
            sage: var('x,y,z')
            (x, y, z)
            sage: P = PoissonBracket([x,y,z], [[0,z,y],[0,0,x],[0,0,0]], make_skew=True)
            sage: P.poisson_matrix
            [ 0  z  y]
            [-z  0  x]
            [-y -x  0]

        Attempt to make a bracket which doesn't satisfy the Jacobi identity.
        ::
            sage: var('x,y,z')
            (x, y, z)
            sage: P = PoissonBracket([x,y,z], [[0,x,y],[0,0,x],[0,0,0]], make_skew=True)
            Traceback (most recent call last):
            ...
            ValueError: poisson_matrix must satisy the Jacobi identity (failed on x, y, z with Jacobiator -y)

        Construct a Poisson bracket over the symbolic ring. We also use
        ``ignore_jacobi`` as otherwise we would raise an error, and
        ``base_ring`` as we can't convert symbolic functions to rationals.
        ::
            sage: var('x,y,z')
            (x, y, z)
            sage: pi_01 = function('pi_01')(x,y,z)
            sage: pi_02 = function('pi_02')(x,y,z)
            sage: pi_12 = function('pi_12')(x,y,z)
            sage: P = PoissonBracket([x,y,z], [[0, pi_01, pi_02], [0,0,pi_12],[0,0,0]], base_ring=SR, make_skew=True) # long time (1.8s)
            Traceback (most recent call last):
            ...
            ValueError: poisson_matrix must satisy the Jacobi identity (failed on x, y, z with Jacobiator -pi_02(x, y, z)...
            sage: P = PoissonBracket([x,y,z], [[0, pi_01, pi_02], [0,0,pi_12],[0,0,0]], make_skew=True, ignore_jacobi=True)
            Traceback (most recent call last):
            ...
            TypeError: unable to convert pi_01(x, y, z) to a rational
            sage: P = PoissonBracket([x,y,z], [[0, pi_01, pi_02], [0,0,pi_12],[0,0,0]], base_ring=SR, make_skew=True, ignore_jacobi=True)
            sage: P.poisson_matrix
            [              0  pi_01(x, y, z)  pi_02(x, y, z)]
            [-pi_01(x, y, z)               0  pi_12(x, y, z)]
            [-pi_02(x, y, z) -pi_12(x, y, z)               0]

        .. SEEALSO::

            :meth:`validate`,
            :mod:`poisson_catalog`
        """
        if isinstance(vars, LieAlgebra):
            lie_algebra = vars

            format = 'matrix'
            gens = lie_algebra.gens()
            R = PolynomialRing(lie_algebra.base_ring(), lie_algebra.variable_names())
            vars = R.gens()

            vectors  = [x.to_vector() for x in gens]
            matrices = [x.adjoint_matrix() for x in gens]
            new_gens_vector = sum(vectors[i] * R.gen(i) for i in range(len(gens)))
            data = Matrix(R, len(gens), len(gens), lambda i, j: vectors[j] * matrices[i] * new_gens_vector)
        else:
            if base_ring == SR:
                R = base_ring
            else:
                R = PolynomialRing(base_ring, vars)
                vars = R.gens()

        super(PoissonBracket, self).__init__(vars)

        self.ignore_jacobi = bool(ignore_jacobi)

        n = len(vars)

        if format == 'detect':
            if is_Matrix(data):
                format = 'matrix'
            else:
                try:
                    m = len(data)
                    if m == n:
                        format = 'matrix'
                    elif m == n - 2:
                        format = 'jacobian'
                    else:
                        raise ValueError("Unable to detect format: data length {}, variables count {}".format(m, n))
                except TypeError:
                    raise ValueError("Unable to determine length of data ({})".format(data))

        if format == 'matrix':
            self.poisson_matrix = Matrix(R, data)
            if make_skew:
                for i in range(n):
                    for j in range(0, i):
                        self.poisson_matrix[i, j] = -self.poisson_matrix[j, i]
                    self.poisson_matrix[i, i] = 0

        if format == 'jacobian':
            if len(data) != n - 2:
                raise ValueError("With format='jacobian', expect data to have length n-2 (expected: {}, got: {})".format(n - 2, len(data)))
            self.poisson_matrix = Matrix(R, nrows=n, ncols=n)
            M = Matrix(R, data)
            for i in range(n):
                for j in range(0, i):
                    self.poisson_matrix[i, j] = -self.poisson_matrix[j, i]
                for j in range(i + 1, n):
                    der_vars = [vars[k] for k in range(n) if not (k == i or k == j)]
                    self.poisson_matrix[i, j] = (-1)**(i+j+1) * jacobian(M, der_vars).determinant()

        self.validate()

    def _latex_(self):
        r"""Return the LaTeX representation of this object.

        EXAMPLES:

        ::
            sage: P = poisson_brackets.SklyaninBracket()
            sage: latex(P) # indirect doctest
            \text{Poisson bracket on } (x, y, z)
        """
        return "\\text{{Poisson bracket on }} {}".format(self.vars)

    def _repr_(self):
        r"""Return a string representing this object.

        EXAMPLES:

        ::
            sage: P = poisson_brackets.ToricBracket()
            sage: print(P._repr_())
            Poisson bracket on (x, y, z)
        """
        return "Poisson bracket on {}".format(self.vars)

    @staticmethod
    def make_ring(base_ring=QQ, *args):
        """Create a PolynomialRing, but don't include variables already in the original ring.

        INPUT:

        - ``base_ring`` -- (default: QQ) the base ring for the new PolynomialRing
        - ``*args`` -- a list of variables or constants to extend the ring by

        OUTPUT:

        A ``PolynomialRing`` containing the variables in ``*args``. If any of
        the expressions in ``*args`` are in fact constants, then we ignore
        them.

        ALGORITHM:

        We check each element of ``*args`` to see if it is in the base ring. If
        it is, then we forget about it. If not, we extend the ring by it. Note
        that the ring is only extended once i.e. we end up with QQ[x,y] rather
        than QQ[x][y].

        EXAMPLES:
        Create a rational polynomial ring.
        ::

            sage: print(PoissonBracket.make_ring(QQ, 'x'))
            Univariate Polynomial Ring in x over Rational Field
            sage: z,w = var('z,w')
            sage: print(PoissonBracket.make_ring(CC, z, w))
            Multivariate Polynomial Ring in z, w over Complex Field with 53 bits of precision

        With no new variables given, just return the original ring.
        ::

            sage: print(PoissonBracket.make_ring(QQ, 42))
            Rational Field

        Create a polynomial ring, but don't include the rational constants we pass.
        ::

            sage: print(PoissonBracket.make_ring(QQ, 'x', 1, 'y'))
            Multivariate Polynomial Ring in x, y over Rational Field
        """
        extra_vars = [var for var in args if not var in base_ring]
        if not extra_vars:
            return base_ring
        return PolynomialRing(base_ring, extra_vars)

    def adjoin(self, var, ham):
        """Create a new Poisson manifold by adjoining a new variable with a
        given Hamiltonian to this structure.

        INPUT:

        - ``var`` -- the variable (or variable name) to add to this structure
        - ``ham`` -- the Hamiltonian vector field for the new variable, as a
          vector

        OUTPUT:

        A Poisson bracket which has the same value for the bracket of
        coordinates in this existing structure, and with {var, x_i} = ham[i].

        The Hamiltonian ``ham`` can be of length n or n+1, where n is the
        dimension of the current manifold. If the dimension is of length n, it
        is taken as given that {var, var} = 0 and this is added in to the
        Hamiltonian automatically.

        EXAMPLES:
        Adjoin in a new variable.
        ::

            sage: P = poisson_brackets.SkewBracket(t=1)
            sage: x, y = P.vars
            sage: z = var('z')
            sage: Q = P.adjoin(z, [1/2*x*z, 1/2*y*z, 0]); Q.poisson_matrix # long time
            [       0  1/2*x*y -1/2*x*z]
            [-1/2*x*y        0 -1/2*y*z]
            [ 1/2*x*z  1/2*y*z        0]

        Adjoin a new variable, not specifying final zero in vector
        ::

            sage: P = poisson_brackets.SkewBracket(t=1)
            sage: x, y = P.vars
            sage: z = var('z')
            sage: Q = P.adjoin(z, [1/2*x*z, 1/2*y*z])
            sage: Q.poisson_matrix
            [       0  1/2*x*y -1/2*x*z]
            [-1/2*x*y        0 -1/2*y*z]
            [ 1/2*x*z  1/2*y*z        0]

        It is an error to not specify enough (or too much) data
        ::

            sage: P = poisson_brackets.SkewBracket(t=1)
            sage: Q = P.adjoin(z, [1])
            Traceback (most recent call last):
            ValueError: Hamiltonian must be of length n or n+1 (expected (n): 2, received: 1)
            sage: Q = P.adjoin(z, [1, 2, 3, 4])
            Traceback (most recent call last):
            ValueError: Hamiltonian must be of length n or n+1 (expected (n): 2, received: 4)

        It is also an error to specify data which would result in a Poisson
        bracket not satisfying the Jacobi identity
        ::

            sage: P = poisson_brackets.JordanBracket()
            sage: P.adjoin(z, [1,1,0])
            Traceback (most recent call last):
            ...
            ValueError: poisson_matrix must satisy the Jacobi identity (failed on x, y, z with Jacobiator 2*x)
        """
        n = self.dimension()
        if len(ham) == n:
            ham = list(ham) + [0]

        ham = vector(ham)

        if len(ham) != n + 1:
            raise ValueError("Hamiltonian must be of length n or n+1 (expected (n): {}, received: {})".format(n, len(ham)))

        R = self.poisson_matrix.base_ring()
        base_R = R.base_ring()
        new_R = PolynomialRing(base_R, self.vars + (var,))

        new_poisson_matrix = Matrix(self.poisson_matrix, ring=new_R)
        new_poisson_matrix = new_poisson_matrix.augment((-1) * vector(ham[:-1]))
        new_poisson_matrix = new_poisson_matrix.stack(ham)

        return PoissonBracket(self.vars + (var,), new_poisson_matrix)

    def central_extension(self, var, delta):
        """Create a central extension of this Poisson bracket.

        INPUT:

        - ``var`` -- the variable (or variable name) to add to this structure
        - ``delta`` -- the amount to change the existing tensor by

        OUTPUT:

        A Poisson bracket on the same variables as the existing one plus a new
        variable ``var``. The new variable is Casimir i.e. {w, -} == 0.
        We also change the tensor of the new Poisson bracket by the matrix
        ``delta``.

        For this to be a central extension, we expect ``delta`` to have
        elements lying in the ideal generated by the new variable. This ensures
        that on the plane $w=0$ the Poisson bracket remains unchanged from the
        original structure.

        EXAMPLES:

        @TODO ask BP if there are any classical examples

        We can accept either a list or matrix as the delta matrix::

            sage: P = poisson_brackets.SkewBracket()
            sage: z = var('z')
            sage: L = [[0, z], [-z, 0]]
            sage: P.central_extension(z, L).poisson_matrix
            [               0    1/2*t*x*y + z                0]
            [(-1/2*t)*x*y - z                0                0]
            [               0                0                0]
            sage: P.central_extension(z, Matrix(L)).poisson_matrix
            [               0    1/2*t*x*y + z                0]
            [(-1/2*t)*x*y - z                0                0]
            [               0                0                0]

        If the type of delta is not one compatible with addition to matrices,
        this will cause an error::

            sage: P = poisson_brackets.SkewBracket()
            sage: z = var('z')
            sage: P.central_extension(z, 0).poisson_matrix
            [           0    1/2*t*x*y            0]
            [(-1/2*t)*x*y            0            0]
            [           0            0            0]
            sage: P.central_extension(z, vector([0, 2]))
            Traceback (most recent call last):
            ...
            TypeError: unsupported operand parent(s) for +: 'Full MatrixSpace of 2 by 2 dense matrices over Multivariate Polynomial Ring in x, y over Univariate Polynomial Ring in t over Rational Field' and 'Ambient free module of rank 2 over the principal ideal domain Integer Ring'

        We check that the resulting bracket is a valid Poisson bracket::

            sage: P = poisson_brackets.SkewBracket()
            sage: z = var('z')
            sage: P.central_extension(z, 1)
            Traceback (most recent call last):
            ...
            ValueError: poisson_matrix must be skew-symmetric
        """
        n = self.dimension()
        new_poisson_matrix = copy(self.poisson_matrix)
        if isinstance(delta, list):
            delta = Matrix(delta)
        new_poisson_matrix += delta
        new_poisson_matrix = new_poisson_matrix.augment(vector([0] * n))
        new_poisson_matrix = new_poisson_matrix.stack(vector([0] * (n + 1)))

        # Use the base ring (i.e the ring without coordinate variables) of this bracket
        R = self.poisson_matrix.base_ring().base_ring()

        return PoissonBracket(self.vars + (var,), new_poisson_matrix, R, format='matrix')

    def validate(self, throw_exception=True):
        """Check if this object is a valid Poisson bracket.

        INPUT:

        - ``throw_exception`` -- (default True) if this evaluates to True,
          raise an exception. Otherwise return a boolean.

        OUTPUT:

        If ``throw_exception`` is True, we raise a ValueError if the bracket is
        not valid, or return nothing if the bracket is valid.

        If ``throw_exception`` is False, return False if this Poisson bracket
        is invalid, or True otherwise.

        A bracket is considered valid if the following conditions are met:

        - The matrix of coefficients is of the correct dimension ($n \times n$,
          where $n$ is the number of coordinate variables)
        - The matrix of coefficients is skew-symmetric
        - The matrix of coefficients satisfies the Jacobi identity

        Note that this final check can be overridden if the ``ignore_jacobi``
        option is passed to the constructor. See the documentation there for
        more details.

        This method is called by the constructor.

        EXAMPLES:
        A valid Poisson tensor
        ::

            sage: var('x,y')
            (x, y)
            sage: P = PoissonBracket([x,y], [[0, 1], [-1, 0]]) # indirect doctest
            sage: print(P.validate(False))
            True

        An example which fails due to having incorrect dimensions
        ::

            sage: var('x,y')
            (x, y)
            sage: P = PoissonBracket([x,y], [[0, 1], [1, 0], [0, 0]], format='matrix') # indirect doctest
            Traceback (most recent call last):
            ...
            ValueError: poisson_matrix must have correct dimension (recieved: (3, 2), expected: (2, 2))

        An example which fails due to not being skew symmetric
        ::

            sage: var('x,y')
            (x, y)
            sage: P = PoissonBracket([x,y], [[0, 1], [1, 0]]) # indirect doctest
            Traceback (most recent call last):
            ...
            ValueError: poisson_matrix must be skew-symmetric

        .. SEEALSO::

            :meth:`__init__`.
        """
        n = self.dimension()
        err_str = None
        if self.poisson_matrix.dimensions() != (n, n):
            err_str = "poisson_matrix must have correct dimension (recieved: {0}, expected: {1})".format(self.poisson_matrix.dimensions(), (n, n))
        elif not self.poisson_matrix.is_skew_symmetric():
            err_str = "poisson_matrix must be skew-symmetric"
        else:
            if n >= 3 and not self.ignore_jacobi:
                jacobiator = self.jacobiator(self.bracket)
                for L in Combinations(range(0, n), 3).list():
                    J = jacobiator(L[0], L[1], L[2])
                    if not J.is_zero():
                        err_str = "poisson_matrix must satisy the Jacobi identity (failed on {0}, {1}, {2} with Jacobiator {3})".format(self.vars[L[0]], self.vars[L[1]], self.vars[L[2]], J)
                        break

        valid = err_str is None
        if throw_exception:
            if not valid:
                raise ValueError(err_str)
        else:
            return valid

    def bracket(self, f, g):
        r"""Calculate the Poisson bracket of two expressions.

        INPUT:

        - ``f`` -- the first expression
        - ``g`` -- the second expression

        OUTPUT:

        The value $\{f, g\}$.

        ALGORITHM:

        We use the fact that $\{f, g\} = \sum_{i, j} \{x_i, x_j\} \partial_{x_i} f \partial_{x_j} g$.

        EXAMPLES:
        ::

            sage: P = poisson_brackets.ToricBracket()
            sage: x, y, z = P.vars
            sage: P.bracket(x, y)
            1/2*p*x*y
            sage: P.bracket(x, x)
            0

        Check a symbolic equation
        ::

            sage: P = poisson_brackets.GenericBracket(2)
            sage: x, y = P.vars
            sage: f = function("f")(x,y)
            sage: g = function("g")(x,y)
            sage: P.bracket(f, g)
            -pi_01(x, y)*diff(f(x, y), y)*diff(g(x, y), x) + pi_01(x, y)*diff(f(x, y), x)*diff(g(x, y), y)

        .. SEEALSO::

          :meth:`__init__` for more examples
        """
        n = self.dimension()

        return sum([self.poisson_matrix[i, j] * self.derivative(f, i) * self.derivative(g, j) for i in range(n) for j in range(n)])

    def tensor_derivative(self, indices, derivatives):
        """Calculate a derivative of one of the coefficients of the tensor.

        INPUT:

        - ``indicies`` -- the component of the tensor to differentiate, in the form [i, j]
        - ``derivatives`` -- a list of derivatives to apply to the coefficient.

        OUTPUT:

        The coefficient of the tensor after the given derivatives have been
        applied.

        EXAMPLES:

        Apply this function to the Sklyanin Poisson bracket.
        ::

            sage: P = poisson_brackets.SklyaninBracket()
            sage: P.poisson_matrix[0, 1]
            t*x*y + z^2
            sage: P.tensor_derivative([0, 1], [1])
            t*x
            sage: P.tensor_derivative([0, 1], [2])
            2*z
        """
        return self.derivative_index(self.poisson_matrix, indices, derivatives)

    def lie_derivative(self, Y):
        n = len(Y)
        if self.poisson_matrix.dimensions() != (n,n):
            raise ValueError("Y must have correct dimension (recieved: {0}, expected: {1})".format(n, self.poisson_matrix.dimensions()[0]))

        def calc_term(i,j,k):
            if i == j and i != k:
                return Y[i] * self.tensor_derivative([j,k],[i]) + self.poisson_matrix[i,k] * Y[j].derivative(self.vars[i])
            elif i == k and i != j:
                return Y[i] * self.tensor_derivative([j,k],[i]) + self.poisson_matrix[j,i] * Y[k].derivative(self.vars[i])
            elif i == j and i == k:
                return Y[i] * self.tensor_derivative([j,k],[i]) + self.poisson_matrix[i,k] * Y[j].derivative(self.vars[i]) + self.poisson_matrix[j,i] * Y[k].derivative(self.vars[i])
            else:
                return Y[i] * self.tensor_derivative([j,k],[i])

        return  Matrix([[sum([calc_term(i,j,k) for i in range(n)]) for j in range(n)] for k in range(n)])

    def hamiltonian(self, f, as_vector=False):
        r"""Return the Hamilonian vector field of f.

        INPUT:

        - ``f`` -- an expression to take the Hamiltonian of
        - ``vector`` -- whether to format the Hamiltonian as a vector

        OUTPUT:

        The Hamiltonian of ``f``, which can be any expression in terms of the
        coordinate variables of the affine space this bracket is defined upon.

        If ``vector`` is True, we return the Hamiltonian as a vector, with each
        entry representing the coefficient of $\partial_{x_i}$. Otherwise, we
        return it as a function. This function is a map from the set of smooth
        functions on the affine space to itself, which gives a vector field on
        the space.

        EXAMPLES:

        Calculate a Hamiltonian as a vector
        ::

            sage: P = poisson_brackets.SklyaninBracket(t=1)
            sage: x, y, z = P.vars
            sage: P.hamiltonian(x, as_vector=True) == vector([0, x*y + z^2, -x*z - y^2])
            True
            sage: P.hamiltonian(x+y, as_vector=True) == vector([-x*y - z^2, x*y + z^2, x^2 - y^2 - x*z + y*z])
            True

        Calculate a Hamiltonian as a function
        ::

            sage: P = poisson_brackets.ToricBracket()
            sage: x, y, z = P.vars
            sage: H = P.hamiltonian(x)
            sage: H(y) == P.bracket(x, y)
            True
        """
        if as_vector:
            return vector([self.bracket(f, self.vars[i]) for i in range(len(self.vars))])
        return partial(PoissonBracket.bracket, self, f)

    def modular_derivation(self, f):
        r"""The derivation defined by the modular vector field with respect to
        the standard coordinate volume form.

        INPUT:

        - ``f`` -- an expression

        OUTPUT:

        The action of the modular vector field on ``f``.  If the Poisson
        tensor is

        $\pi = \sum_{i,j} \pi^{1,j} \partial_{x_i} \wedge \partial_{x_j}$,

        it returns the coefficients of the vector field

        $\sum_{i,j} \frac{\partial \pi^{i,j}}{\partial_{x_i}} \frac{\partial f}{\partial_{x_j}}$

        EXAMPLES:
        ::

            sage: P = poisson_brackets.ToricBracket()
            sage: x, y, z = P.vars
            sage: P.modular_derivation(x)
            (-1/2*p + 1/2*q)*x


        """
        return sum([sum([self.derivative(self.poisson_matrix[i, j], i) for i in range(len(self.vars))]) * self.derivative(f, j) for j in range(len(self.vars))])

    def modular_vector_field(self,):
        r"""Returns the components of the modular vector field with respect to
        the standard coordinate volume form.

        OUTPUT:

        If the Poisson tensor is

        $\pi = \sum_{i,j} \pi^{1,j} \partial_{x_i} \wedge \partial_{x_j}$,

        it returns the coefficients of the vector field

        $\sum_{i,j} \frac{\partial \pi^{i,j}}{\partial_{x_i}} \partial_{x_j}$.

        EXAMPLES:
        ::

            sage: P = poisson_brackets.ToricBracket()
            sage: P.modular_vector_field()
            ((-1/2*p + 1/2*q)*x, (1/2*p - 1/2*r)*y, (-1/2*q + 1/2*r)*z)

        """
        return vector([self.modular_derivation(self.vars[i]) for i in range(len(self.vars))])

    def pfaffian_ideal(self, d):
        """Return the ideal generated by the Pfaffians of all $d \times d$
        skew-symmetric submatrices of this tensor.

        INPUT:

        - ``d`` -- the dimension of the submatrices to take

        OUTPUT:

        The Pfaffian ideal.

        This ideal defines the locus of symplectic leaves of dimension less than
        ``d``. We require d to be an even integer between 2 and the dimension;
        if this condition is not met, we raise an exception.

        @TODO ask BP if there are any famous examples

        EXAMPLES:

        Calculate the 2-dimensional Pfaffian of the Sklyanin algebra
        ::

            sage: w = var('w')
            sage: P = poisson_brackets.SklyaninBracket()
            sage: P.pfaffian_ideal(2)
            Ideal (t*x*y + z^2, -y^2 + (-t)*x*z, x^2 + t*y*z) of Multivariate Polynomial Ring in x, y, z over Univariate Polynomial Ring in t over Rational Field

        Try an invalid dimension
        ::

            sage: P = poisson_brackets.SkewBracket()
            sage: P.pfaffian_ideal(4)
            Traceback (most recent call last):
            ...
            ValueError: Size of the submatrices must be an even integer d with 2 <= d <= 2
        """

        if (d > self.dimension()) or (d < 2) or (d % 2 != 0):
            raise ValueError("Size of the submatrices must be an even integer d with 2 <= d <= {}".format(self.dimension()))

        indices = Combinations(range(0, self.dimension()), d).list()
        return Ideal([self.poisson_matrix.matrix_from_rows_and_columns(L, L).pfaffian() for L in indices])
