r"""
Merges a .mpl file of graph weights into a .sobj file.

If the output file does not exist, it will be created.

Example usage:
  sage import_maple.py --maple log_5_2.mpl --output log.sobj
"""

import argparse
import os

from all import *

parser = argparse.ArgumentParser()

parser.add_argument("-m", "--maple", help="Maple file of cached graph values", required=True)
parser.add_argument("-o", "--output", help="output .sobj file. Will merge in new values if the file already exists", required=True)

args = parser.parse_args()
outfile = args.output

if not outfile:
    raise ValueError("Output should be a .sobj file")

if os.path.isfile(outfile):
    kontsevichgraphs.load_cache(outfile)
else:
    kontsevichgraphs.clear_cache()
kontsevichgraphs.merge_maple_cache(args.maple)
kontsevichgraphs.save_cache(outfile)
