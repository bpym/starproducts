r"""Allow the calculation of the Kontsevich star product.

AUTHORS:

- Peter Banks (2016-08-29): initial version

EXAMPLES:
Calculate the star product of the skew algebra
::

    sage: P = poisson_brackets.SkewBracket()
    sage: x, y = P.vars
    sage: S = StarProduct(P)
    sage: S.star(x,y,2) # long time (1.8s)
    1/24*h^2*t^2*x*y + 1/2*h*t*x*y + x*y

The following examples/test verify the correctness of the weight caches.
Calculate the point scheme associated to the Sklyanin algebra
::

    sage: P = poisson_brackets.SklyaninBracket()
    sage: S = StarProduct(P)
    sage: C, A = S.point_scheme(); C # long time (5s)
    [4/43046721*hbar*x^3 + 4/43046721*hbar*y^3 + 4/14348907*t*hbar*x*y*z + 4/43046721*hbar*z^3]

Calculate star product for log propagator:
::
    sage: kontsevichgraphs.load_cache('logarithmic') # long time
    sage: P = poisson_brackets.SkewBracket(t=1)
    sage: x, y = P.vars
    sage: StarProduct(P, default_order=6).star(x,y) # long time
    101/580608*h^6*x*y + 1/768*h^5*x*y - 13/5760*h^4*x*y - 1/48*h^3*x*y + 1/24*h^2*x*y + 1/2*h*x*y + x*y
    sage: kontsevichgraphs.load_cache('default')

Calculate star product for harmonic propagator:
::
    sage: kontsevichgraphs.load_cache('harmonic') # long time
    sage: P = poisson_brackets.SkewBracket(t=1)
    sage: x, y = P.vars
    sage: StarProduct(P, default_order=6).star(x,y) # long time
    1/480*h^5*x*y - 1/1440*h^4*x*y - 1/48*h^3*x*y + 1/24*h^2*x*y + 1/2*h*x*y - 1/184320*(17*pi^6 - 22590*zeta(3)^2)*h^6*x*y/pi^6 + x*y
    sage: kontsevichgraphs.load_cache('default')

Calculate star product for tfamily propagator:
::
    sage: kontsevichgraphs.load_cache('tfamily') # long time
    sage: P = poisson_brackets.SkewBracket(t=1)
    sage: x, y = P.vars
    sage: StarProduct(P, default_order=6).star(x,y) # long time
    -1/11520*(64*t^4 - 128*t^3 + 116*t^2 - 52*t - 15)*h^5*x*y - 1/5760*(64*t^4 - 128*t^3 + 116*t^2 - 52*t + 13)*h^4*x*y - 1/48*h^3*x*y + 1/24*h^2*x*y + 1/2*h*x*y - 1/2903040*(896*(851*pi^6 - 521235*zeta(3)^2)*t^8 - 3584*(851*pi^6 - 521235*zeta(3)^2)*t^7 + 16*(277051*pi^6 - 168376320*zeta(3)^2)*t^6 - 505*pi^6 - 16*(163969*pi^6 - 96480720*zeta(3)^2)*t^5 + 80*(1711*pi^6 - 205254*zeta(3)^2)*t^4 + 1296*(417*pi^6 - 278180*zeta(3)^2)*t^3 - 4*(57803*pi^6 - 36276660*zeta(3)^2)*t^2 + 972*(33*pi^6 - 19460*zeta(3)^2)*t)*h^6*x*y/pi^6 + x*y
    sage: kontsevichgraphs.load_cache('default')
"""
from copy import copy
from functools import partial, reduce
import operator
import time

from sage.algebras.free_algebra import FreeAlgebra
from sage.functions.other import conjugate, factorial
from sage.matrix.constructor import Matrix
from sage.misc.latex import latex
from sage.modules.free_module_element import vector
from sage.rings.integer_ring import ZZ
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.rings.rational import Rational
from sage.structure.sage_object import SageObject
from sage.symbolic.operators import add_vararg
from sage.symbolic.ring import SR

# When run as part of Sage
from graphs import kontsevichgraphs

# When run as stand-alone
#from graphs.all import kontsevichgraphs

def _split_expr(expr):
    monomials = None
    coeffs = None
    try:
        coeffs = expr.coefficients()
        monomials = expr.monomials()
    except Exception:
        try:
            if expr.operator() == add_vararg:
                monomials = expr.operands()
                coeffs = [1 for _M in monomials]
            else:
                monomials = [expr]
                coeffs = [1]
        except Exception as e:
            raise e

    return monomials, coeffs


def _calc_term(weight, graph, aevf, P, f, g):
    """Calculate the term in the star product.

    INPUT:

    - ``weight`` - the precomputed weight of the graph
    - ``graph`` - the list of graph
    - ``aefv`` - (are external vertices fixed) whether external vertices are fixed.
    - ``P`` - the Poisson bracket we are calculating the star product of
    - ``f``, ``g`` - the functions we are calculating the star product of
    - ``i`` - the index in the list of graph to use

    OUTPUT:

    The contribution to the star product of graph ``i`` in the list
    ``graph``.
    """
    try:
        if aevf:
            return weight * graph.diffop(P, f, g) + graph.flip_factor() * conjugate(weight) * graph.diffop(P, g, f)
        else:
            return weight * 2 * graph.diffop(P, f, g)
    except Exception:
        return None


class StarProduct(SageObject):
    r"""A class to allow calculate of the Kontsevich star product.

    In [Kont1998]_, Kontsevich defined a method of
    deforming a commutative product on an algebra to give a non-commutative
    star product. This star product is written in terms of an indeterminate
    ``h``, which is treated as a small parameter.

    This star product is written in terms of a Poisson bracket. Given such a
    structure, the coefficient of $h^n$ in the star product is determined by a
    sum over a series of graphs, each graph contributing a weight and a
    differential operator which is applied to the operands of the star product.

    This class allows the computation of the star product from any given
    Poisson bracket, performing the necessary calculations. As it is not yet
    known how to compute an exact expression for the star product, we calculate
    the first few terms instead.

    ALGORITHM:
    The brunt of the work is handled by the :class:`KontsevichGraph` class. This
    stores a graph and calculates its weight. In this class, we store a list of
    such graphs and cache their weight. Having done this, when asked to
    calculate a star product we apply the differential operators as needed and
    sum the results, ignoring any graphs with zero weight.

    .. SEEALSO::

      :class:`poisson.PoissonBracket`,
      :class:`kontsevich.KontsevichGraph`.

    REFERENCES:

    .. [Kont1998] \M. Kontsevich. Deformation quantization of Poisson
       manifolds, 1998.
    """

    # The default default calculation order
    _default_order = 3

    def __init__(self, P, h=None, default_order=None):
        """Create a new star product based on a Poisson bracket.

        INPUT:

        - ``P`` -- the Poisson bracket that we will deform
        - ``h`` -- (default ``None``) a variable that represents the small
          parameter. If ``None``, a new symbolic variable named 'h' is created
        - ``default_order`` -- (default 3) the default order to calculate the star
          product to (inclusive)

        OUTPUT:

        An object capable of calculating the star product of the given Poisson
        bracket.

        EXAMPLES:

        Create a new StarProduct based on a toric Poisson bracket.
        ::

            sage: S = StarProduct(poisson_brackets.ToricBracket())
            sage: x, y, z = S.P.vars
            sage: S.star(y, z, order=2)
            1/24*(p*q + r^2)*h^2*y*z + 1/2*h*r*y*z + y*z

        Create a new StarProduct with a low default order.
        ::

            sage: S = StarProduct(poisson_brackets.SklyaninBracket(), default_order=1)
            sage: x, y, z = S.P.vars
            sage: bool(S.star(x, y) == S.P.bracket(x, y)*S.h + x*y)
            True

        Create a StarProduct with a different variable name.
        ::

            sage: S = StarProduct(poisson_brackets.SkewBracket(), h=var('q'))
            sage: x, y = S.P.vars
            sage: S.star(x, y)
            -1/48*q^3*t^3*x*y + 1/24*q^2*t^2*x*y + 1/2*q*t*x*y + x*y
        """
        self._cache_filename = None
        self.P = P
        self.h = h if h is not None else SR.var('h')

        if default_order is None:
            self.default_order = StarProduct._default_order
        else:
            self.default_order = default_order

        self.filter = kontsevichgraphs.filter_weightless

        # A list of all graphs we need to consider, along with their weights
        # (we include a factor of G.iso_class_size() in their weight)
        # We also cache the results of are_external_vertices_fixed
        self.graphs = []
        self.weights = []
        self.aevf = []

        self._cache = {}

    def _latex_(self):
        """Return a LaTeX representation of this object.

        EXAMPLES::

            sage: P = poisson_brackets.SklyaninBracket()
            sage: h = var('h')
            sage: S = StarProduct(P, h)
            sage: latex(S) # indirect doctest
            \text{Star product in } h \text{ over } \text{Poisson bracket on } (x, y, z)
        """
        return "\\text{{Star product in }} {} \\text{{ over }} {}".format(latex(self.h), latex(self.P))

    def _repr_(self):
        """Return a LaTeX representation of this object.

        EXAMPLES::

            sage: P = poisson_brackets.SklyaninBracket()
            sage: h = var('h')
            sage: S = StarProduct(P, h)
            sage: print(S._repr_())
            Star product in h over Poisson bracket on (x, y, z)
        """
        return "Star product in {} over {}".format(self.h, self.P)

    def _init_weights(self, n):
        r"""Initialise the list of graphs and weights up to order n.

        INPUT:

        - ``n`` - the order up to which to calculate (inclusive)

        OUTPUT:

        None. We set ``self.graphs`` and ``self.weights`` to be a list of
        lists of graphs and weights, such that ``self.graphs[k]`` is a list of all
        relevant order-k graphs.

        The weights include a factor of 1/2 * isomorphism_class_size so we do
        not need to recalculate these each time.

        EXAMPLES:

        Initialise some low-order weights
        ::

            sage: S = StarProduct(poisson_brackets.SkewBracket())
            sage: S._init_weights(3)
            sage: S.graphs
            [[Kontsevich graph (Quantization) on (0, 2) vertices],
             ...
              Kontsevich graph (Quantization) on (3, 2) vertices]]
            sage: S.weights
            [[1/2],
             [1/2],
             [-2/3, -1/6, 1/2],
             [1, 1, 1, -2, -1, 1/2, 1/2, -1/2]]
        """
        if self._cache_filename is None:
            self._cache_filename = kontsevichgraphs._cache_filename

        start = len(self.graphs)
        if start >= n+1:
            return

        if self._cache_filename != kontsevichgraphs._cache_filename:
            raise RuntimeError("The graph weight cache has changed. This object can no longer be safely used to calculate star products.")

        for i in range(start, n + 1):
            self.graphs.append(list(kontsevichgraphs.list(i, graph_filter=self.filter)))
            self.weights.append([Rational((1, 2)) * G.weight() * G.isomorphism_class_size() for G in self.graphs[i]])
            self.aevf.append([G.are_external_vertices_fixed() for G in self.graphs[i]])

    def star_term(self, f, g, k):
        r"""Calculate an individual term in the star product.

        INPUT:
        - ``f``, ``g`` -- the functions to compute the star product of
        - ``k`` -- the order of the term to calculate

        OUTPUT:

        The h**k term in the star product, including a factor of h**k.

        Although this function can be used, it is generally expected that
        the user would use :meth:`star` to generate the whole power series.

        EXAMPLES:
        ::

            sage: S = StarProduct(poisson_brackets.ToricBracket(), var('h'))
            sage: x, y, z = S.P.vars
            sage: S.star_term(x, y, 3) # long time (1.5s)
            -1/48*(p^3 - p*q*r)*h^3*x*y

        Generate the Moyal product (terms taken from [Kontsevich2003])
        ::

            sage: S = StarProduct(poisson_brackets.DarbouxBracket())
            sage: x, y = S.P.vars
            sage: d = derivative
            sage: A = function('A')(x, y)
            sage: B = function('B')(x, y)
            sage: moyal  = A * B
            sage: moyal += h * (d(A, x) * d(B, y) - d(A, y)*d(B,x))
            sage: moyal += h^2/2 * (d(A, x, x) * d(B, y, y) - 2 * d(A, x, y) * d(B, x, y) + d(A, y, y) * d(B, x, x))
            sage: moyal += h^3/6 * (d(A, x, 3) * d(B, y, 3) - 3 * d(A,x,x,y) * d(B,x,y,y) + 3*d(A,x,y,y)*d(B,y,x,x) - d(A, y,y,y) * d(B, x,x,x))
            sage: bool(S.star(A, B, 3) == moyal) # long time
            True

        It is an error to provide a negative or non-integer value for ``k``
        ::

            sage: S = StarProduct(poisson_brackets.ToricBracket(), var('h'))
            sage: x, y, z = S.P.vars
            sage: S.star_term(x, y, -1)
            Traceback (most recent call last):
            ...
            ValueError: k must be a non-negative integer (received: -1)

        .. SEEALSO::

          :meth:`star`

        TESTS:

        Check a generic bracket generates known formula up to order 3. The known
        expression is taken from Dito's paper.
        ::

            sage: P2 = poisson_brackets.GenericBracket(2)
            sage: x, y = P2.vars
            sage: pi_01 = P2.poisson_matrix[0,1]
            sage: S2 = StarProduct(P2, h)

            sage: d = derivative

            sage: e = A*B
            sage: e += h*pi_01*(d(A, x)*d(B, y) - d(A, y) * d(B, x))
            sage: e -= h**2 * 1/6 * (d(pi_01, y)**2 * d(A, x) * d(B, x)-d(pi_01, x)*d(pi_01, y) * (d(A, x) * d(B, y) + d(A, y)*d(B, x)) + d(pi_01, x)**2 * d(A, y) * d(B, y))
            sage: e += h**2 * 1/3 * pi_01 * (d(pi_01, y)*(d(A, x, x) * d(B, y) - d(A, x, y)*d(B, x)) - d(pi_01, x) * (d(A, x, y) * d(B, y) - d(A, y, y)*d(B, x)))
            sage: e += h**2 * 1/3 * pi_01 * (d(pi_01, y)*(d(B, x, x) * d(A, y) - d(B, x, y)*d(A, x)) - d(pi_01, x) * (d(B, x, y) * d(A, y) - d(B, y, y)*d(A, x)))
            sage: e += h**2 * 1/2 * pi_01**2 * (d(A, x, x) * d(B, y, y) - 2*d(A, x, y)*d(B, x, y) + d(A, y, y)*d(B, x, x))
            sage: bool(S2.star(A, B, 2) == e) # long time
            True
        """
        if not k in ZZ or k < 0:
            raise ValueError("k must be a non-negative integer (received: {})".format(k))

        if (f, g, k) not in self._cache:
            self._init_weights(k)

            graphs = self.graphs[k]
            weights = self.weights[k]
            # are external vertices fixed
            aevf = self.aevf[k]

            terms = [_calc_term(*a, self.P, f, g) for a in zip(weights, graphs, aevf)]
            self._cache[f, g, k] = sum(filter(None, terms)) * (self.h**k) * Rational((1, factorial(k)))

        return self._cache[f, g, k]

    def star(self, f, g, order=None):
        r"""Calculate the star product up to a given order.

        INPUT:

        - ``f``, ``g`` -- the functions to compute the star product of
        - ``order`` -- (default specified by constructor) the order to compute to

        OUTPUT:

        The terms of f*g up to and including order ``order``.

        If ``order`` is negative, we raise an exception.

        EXAMPLES:

        Check an example believed to give an exponential series
        ::

            sage: S = StarProduct(poisson_brackets.ToricBracket(), var('h'))
            sage: x, y, z = S.P.vars
            sage: xy = S.star(x, y) # long time (1.5s)
            sage: yx = S.star(y, x) # long time (1.5s)
            sage: taylor(xy/yx, h, 0, 3) # long time (1.5s)
            1/6*h^3*p^3 + 1/2*h^2*p^2 + h*p + 1

        It is an error for ``order`` to be negative
        ::

            sage: S = StarProduct(poisson_brackets.ToricBracket(), var('h'))
            sage: S.star(x, y, -1)
            Traceback (most recent call last):
            ...
            ValueError: Order must be a non-negative integer (received: -1)

        NOTE:

        We compute the term by summing over ``range(order+1)``, and so this
        function will give an output for rational values of ``order`` as if it
        had been called with ``floor(order)``.

        TESTS::

        Check that the star product is associative up to order StarProduct._default_order.

        ::
            sage: from functools import partial
            sage: max_order = 3
            sage: h = var('h')
            sage: test_brackets = {K: poisson_brackets.dictionary[K]() for K in poisson_brackets.dictionary}
            sage: test_products = {K: StarProduct(test_brackets[K], h, default_order=max_order) for K in test_brackets}
            sage: for K in test_products: # long time
            ....:     vars = test_brackets[K].vars
            ....:     s = partial(StarProduct.star, test_products[K])
            ....:     for A in vars:
            ....:         for B in vars:
            ....:             for C in vars:
            ....:                 assoc = s(A, s(B, C)) - s(s(A, B), C)
            ....:                 assoc_trunc = taylor(assoc, (h, 0), max_order)
            ....:                 assert(assoc_trunc.simplify() == 0)
        """
        if order is None:
            order = self.default_order

        if order < 0:
            raise ValueError("Order must be a non-negative integer (received: {})".format(order))

        # Split up into simpler parts if possible
        monomials, coeffs = _split_expr(f)
        if monomials != None:
            if len(monomials) > 1:
                return sum(C * self.star(M, g, order) for C, M in zip(coeffs, monomials))
            if len(monomials) == 0:
                return 0
            if coeffs[0] != 1:
                return coeffs[0]*self.star(monomials[0], g, order)

        monomials, coeffs = _split_expr(g)
        if monomials != None:
            if len(monomials) > 1:
                return sum(C * self.star(f, M, order) for C, M in zip(coeffs, monomials))
            if len(monomials) == 0:
                return 0
            if coeffs[0] != 1:
                return coeffs[0]*self.star(f, monomials[0], order)

        self._init_weights(order)

        terms = [self.star_term(f, g, k) for k in range(order + 1)]

        result = sum([term.full_simplify().collect_common_factors() for term in terms if term != 0])

        return result.collect(self.h)

    def relations(self, order=None, format='tensor'):
        r"""Calculate the relations imposed on an algebra given a star product.

        INPUT:

        - ``order`` -- (default specified by constructor) the order to work to
        - ``format`` -- (default 'tensor') a string specifying the format of the result

        OUTPUT:

        A list of relations that the star product imposes on this algebra. When
        we have a deformed product on a vector space $V$, we can ask what relations
        the product must satisfy. These relations can be thought of as sitting
        inside $V \tensor V$. These relations form an ideal, so we can take the
        quotient of $V \tensor V$ to give a new ring.

        If ``format`` is set to 'vector', we return the relations as a matrix,
        with each row in the matrix specifying a relation. This is written with
        respect to the basis $(x_1x_1, \ldots, x_1x_n, x_2x_1, \ldots,
        x_nx_n)$.

        If ``format`` is set to 'tensor', we return the relation as a list of
        tensors inside $V \tensor V$. As Sage does not support tensor products,
        we write these as elements of a free algebra instead.

        The result will be in terms of the indeterminate $h$. However, as the
        relations are only valid up to the specified order, this element $h$
        lives in a quotient ring. As such, its name will appear as ''hbar''
        when written to the console.

        ALGORITHM:

        We do this by noting that the relations are the kernel of the $n(n+1)/2$
        by $n^2$ matrix given by all possible star products of coordinate
        variables with themselves. Each column in this matrix represents
        $x_i*x_j$ for some $i, j$, with the element of the rows being the
        coefficients of $x_kx_l$ in this expansion for some $k, l$. Thus this
        matrix can be seen as the natural map from $V \tensor V$ to $Sym(V)^2$,
        taken with respect to a given basis.

        Taking the kernel of this matrix then gives us a set of vectors which
        each specify a relation that must be satisfied. The vectors give
        elements in $V \tensor V$ which are precisely the relations.

        We create a new free algebra given by extending the base ring of the
        Poisson bracket by free variables corresponding to the coordinates of
        the Poisson bracket.

        EXAMPLES:
        An example of an exact, known star product
        ::

            sage: S = StarProduct(poisson_brackets.JordanBracket(), var('h'))
            sage: rels = S.relations(); rels[0] # long time (1.5s)
            2*hbar*x^2 - x*y + y*x

        It is conjectured these are exponential relations. We use the vector
        format as we want access to the coefficients.
        ::

            sage: S = StarProduct(poisson_brackets.SkewBracket(), var('h'))
            sage: rels = S.relations(format='vector')
            sage: rels[0][1]/rels[0][2]
            1/6*t^3*hbar^3 - 1/2*t^2*hbar^2 + t*hbar - 1

        It is an error for ``order`` to not be a non-negative integer
        ::

            sage: S = StarProduct(poisson_brackets.SkewBracket(), var('h'))
            sage: S.relations(-1)
            Traceback (most recent call last):
            ...
            ValueError: Order must be a non-negative integer (received: -1)
        """

        n = len(self.P.vars)
        if order is None:
            order = self.default_order
        if not order in ZZ or order < 0:
            raise ValueError("Order must be a non-negative integer (received: {})".format(order))

        coordinate_ring = self.P.poisson_matrix.base_ring()
        # We take the fraction field so we definitely have a PID
        #
        # We'd like to take the quotient by h^order here, but then we end up
        # with something which isn't an integral domain and so taking the
        # kernel is not fun. We instead take the quotient later on in the code
        #
        # We'd also like to only take the fraction field of coordinate_ring,
        # but this can lead to problems about inexact division
        R = PolynomialRing(coordinate_ring, self.h).fraction_field()

        coord_vars = self.P.vars

        M = Matrix(ring=R, nrows=n * (n + 1) // 2, ncols=n**2)
        for i in range(n):
            for j in range(n):
                term = self.star(coord_vars[i], coord_vars[j], order)
                index = 0
                for k in range(n):
                    for l in range(k, n):
                        M[index, i * n + j] = term.coefficient(coord_vars[k] * coord_vars[l])
                        index = index + 1

        rels = copy(M.right_kernel_matrix())

        # Clear denominators
        rels_count = rels.dimensions()[0]
        rels_len = rels.dimensions()[1]
        for i in range(rels_count):
            for j in range(rels_len):
                D = rels[i][j].denominator()
                if D != 1:
                    rels[i] *= D

        # Convert out of fraction field
        R = PolynomialRing(coordinate_ring.fraction_field(), self.h).quo(self.h**(order + 1))
        rels = [vector([R(r) for r in rel]) for rel in rels]

        if format == 'vector':
            # If format == vector, return a matrix.
            return Matrix(rels)
        else:
            # If format == 'tensor', return a list of tensors.
            A = FreeAlgebra(R, n, coord_vars)
            coord_vars = A.gens()
            basis = [a * b for a in coord_vars for b in coord_vars]

            return [sum([A(relation[i]) * basis[i] for i in range(n**2)]) for relation in rels]

    @staticmethod
    def _promote_to_SR(A):
        """Convert an element to the symbolic ring.

        INPUT:

        - ``A`` -- an expression to convert to the symbolic ring. This should
          be a member of (R[h]/h**order)[x1,...,xn] for some ring R.

        OUTPUT:

        The expression ``A`` converted to the symbolic ring

        EXAMPLES::

            sage: R = QQ
            sage: S = PolynomialRing(R, var('h'))
            sage: h = S.gens()[0]
            sage: S = S.quo(h**4)
            sage: Q = PolynomialRing(S, var('x,y'))
            sage: h = S.gens()[0]
            sage: x, y = Q.gens()
            sage: StarProduct._promote_to_SR(x**2 * h + y * h**2).parent()
            Symbolic Ring
        """
        # This should be (QQ[x,y][h]/h**order)[x,y]
        R = A.parent()
        # This should be QQ[x,y][h]/h**order
        Q = R.base_ring()

        new_R = PolynomialRing(Q.ambient(), R.gens())
        new_gens = new_R.gens()

        def to_SR(c, var):
            """Convert a variable to a symbol in the Symbolic Ring"""
            new_c = Q.lift(c)
            exponents = var.exponents()[0]
            new_var = reduce(operator.mul, [new_gens[k] ** exponents[k] for k in range(len(exponents))])
            return SR(new_c * new_var)

        return sum([to_SR(c, var) for c, var in A])

    def point_scheme(self, order=None, convert_to_SR=False):
        """Calculate the point scheme of this star product.

        INPUT:

        - ``order`` -- (default set by constructor) the order to calculate up to
        - ``convert_to_SR`` -- (default False) if True, convert the automorphism to SR

        OUTPUT:

        A tuple, with the first part being the conditions imposed on the point
        scheme by the relations generated by this star product, and the second
        being the automorphism associated to the scheme.

        Normally, the automorphism will live in the base ring of the Poisson
        bracket, extended by h, quotiented out by a given order, and then
        extended again by the coordinate variables. If ``convert_to_SR`` is set
        to ``True``, the automorphism is converted to the symbolic ring.

        EXAMPLES::

            sage: S = StarProduct(poisson_brackets.ToricBracket(), var('h'))
            sage: S.point_scheme(order=2)[0][0] # long time
            ((p + q + r)*hbar)*x*y*z

        It is an error for ``order`` to not be a non-negative integer
        ::

            sage: S = StarProduct(poisson_brackets.ToricBracket(), var('h'))
            sage: C, A = S.point_scheme(order=-1)
            Traceback (most recent call last):
            ...
            ValueError: Order must be a non-negative integer (received: -1)

        We can convert to the symbolic ring
        ::

            sage: S = StarProduct(poisson_brackets.SkewBracket(), var('h'))
            sage: C, A = S.point_scheme(order=2, convert_to_SR=True); A # long time
            (-1/24*(h^2*t^2 - 12*h*t + 24)*x, -1/24*(h^2*t^2 + 12*h*t + 24)*y)

        .. SEEALSO::

            :mod:`point_scheme`
        """
        from .point_scheme import rels_to_point_scheme

        R = self.relations(order)
        C, A = rels_to_point_scheme(R)
        if convert_to_SR:
            A = vector([StarProduct._promote_to_SR(a) for a in A])
        return C, A
