from sage.all import *
from all import *

# Computing examples of Poisson structures and star products

var('h') # Planck constant
var('w,x,y,z') # Coordinates
var('t') # Parameters for brackets
n = 3 # order of star product expansion

##################
# 2d examples
##################

# Skew polynomial
var('t') # parameter
P_skew = poisson_brackets.SkewBracket([x,y], t)
S_skew = StarProduct(P_skew,h)

q = S_skew.star(x,y,n) / S_skew.star(y,x,n)
# Should give exp(ht)xy
print('Skew polynomial ring:  x*y/y*x =' , q.taylor((h,0),n))

# Jordan plane
P_Jordan = poisson_brackets.JordanBracket([x,y])
S_Jordan = StarProduct(P_Jordan,h)

S = S_Jordan
q = S.star(x,y,n) - S.star(y,x,n) - 2*h*S.star(x,x,n-1)
# Should give 0
print('Jordan plane: x*y-y*x - 2h x^2 =' , q.taylor((h,0),n))

##################
# 3d examples
##################

# Toric
var('p,q,r')
P_toric = poisson_brackets.ToricBracket([x,y,z], [p, q, r])
S_toric = StarProduct(P_toric,h)

q = S_toric.star(x,y,n) / S_toric.star(y,x,n)
# Should give exp(hp)
print('Toric 3-fold: x*y/y*x =' , q.taylor((h,0),n))

# 3d Sklyanin
P_Skly = poisson_brackets.SklyaninBracket([x,y,z], t)
S_Skly = StarProduct(P_Skly,h)

q = S_Skly.star(x,y,n) - S_Skly.star(y,x,n) - 2*h*(t*x*y + z**2)
# Should give 0
print('Sklyanian: x*y-y*x-2*h*(t*x*y + z**2) =' , q.taylor((h,0),n))

##################
# Point schemes
##################
example_point_schemes = False
if example_point_schemes:
	base_ring = PolynomialRing(QQ, 'p, q, r')
	p, q, r = base_ring.gens()

	A2 = FreeAlgebra(base_ring, 2, 'x, y')
	x,y = A2.gens()

	tests2 = [
		[ -q*x*y + y*x ],       # Quantum plane
		[ x*y - y*x + x*x ],    # Jordan plane
	]

	A3 = FreeAlgebra(base_ring, 3, 'x, y, z')
	x, y, z = A3.gens()
	tests3 = [
		[ y*x - p*x*y, z*y - q*y*z, x*z - r*z*x ],                              # Quantum 2-space
		[ p*x*y + q*y*x + r*z*z, p*y*z + q*z*y + r*x*x, p*z*x + q*x*z + r*y*y ] # Skylanin
	]

	# Currently all tests work *except* calculating the automorphism for quantum 2-space.
	# This is due to Sage not correctly handling the assumption that arises (that xyz == 0)
	for T in tests2 + tests3:
		print(rels_to_point_scheme(T))
