import os

from collections import defaultdict

from sage.all import I
from sage.combinat.combination import Combinations
from sage.combinat.permutation import Permutation
import sage.env
from sage.functions.other import binomial, factorial, conjugate
from sage.functions.generalized import sgn
from sage.functions.trig import sin, cos
from sage.graphs.digraph import DiGraph
from sage.graphs.generic_graph import GenericGraph
from sage.misc.misc_c import prod
from sage.misc.latex import latex
from sage.rings.rational import Rational
from sage.symbolic.ring import SR

from .partition import PartitionDiGraph, PartitionGenericGraph

MAPLE_DIR = os.path.join(sage.env.SAGE_EXTCODE, 'maple', 'kontsevichgraph_weight', 'kontsevint')

def koszul_sign(sigma, degrees):
    r"""Calculate the Koszul sign of a permutation of graded elements.

    This is defined as follows. Say ${x_1, \ldots, x_n}$ are homogeneous
    elements of a Z-graded vector space, which is graded commutative i.e.
    $xy = (-1)^{|x||y|} yx$. Let $\sigma$ be a permutation of ${1, \ldots, n}$.
    Then the Koszul sign is the sign E in the expression

    \[ x_1 x_2 \cdots x_n = E x_{1\sigma} \cdots x_{n\sigma} \]

    We assume ``degrees`` are a list of integers. They may also be
    indeterminates, in which case the result is returned as an expression in
    these variables, which when taken as the exponent of -1 gives the desired
    sign.

    EXAMPLES:

    Consider x, y, z of degrees 1, 1, 0 respectively. Then xyz = -yxz = -yzx

    ::
        sage: from graphs.kontsevichgraph import koszul_sign
        sage: degrees = [1,1,0]
        sage: koszul_sign([2, 1, 3], degrees)
        -1
        sage: koszul_sign([2, 3, 1], degrees)
        -1

    Generically, we have

    ::
        sage: from graphs.kontsevichgraph import koszul_sign
        sage: degrees = SR.var('x_0,x_1,x_2')
        sage: koszul_sign([2, 1, 3], degrees)
        x_0*x_1
        sage: koszul_sign([2, 3, 1], degrees)
        x_0*x_1 + x_0*x_2
    """
    if not isinstance(sigma, Permutation):
        sigma = Permutation(sigma)

    if not sigma.size() <= len(degrees):
        raise RuntimeError("Require a list of degrees of length at least size of permutation")

    # We split sigma into transpositions
    cycles = sigma.to_cycles(singletons=False)

    signs = [0]*len(degrees)

    for i, degree in enumerate(degrees):
        try:
            signs[i] = degree % 2
        except TypeError:
            signs[i] = degree
    string = list(range(len(sigma)))
    # even for +1, odd for -1
    sign = 0
    for cycle in cycles: # current cycle (1-indexed)
        for i in range(1, len(cycle)):
            transposition = (cycle[0]-1, cycle[i]-1) # current transposition (0-indexed)
            pos = sorted((string.index(x) for x in transposition))
            sign0 = signs[transposition[0]]
            sign1 = signs[transposition[1]]
            if sign0 == sign1:
                # If both even, no change to sign
                # If both odd, a change by factor of (-1)**{2n+1} for some n
                sign += sign0
            else:
                sign += (sign0+sign1)*sum(signs[string[j]] for j in range(pos[0]+1, pos[1])) + sign0*sign1

            # Update string
            string[pos[0]], string[pos[1]] = transposition[1], transposition[0]
    try:
        return 1 if sign % 2 == 0 else -1
    except TypeError:
        return sign

class KontsevichAdmissibleGraph(PartitionDiGraph):
    """An admissible graph, as defined in [DefQuant]."""
    # Internal vertices 1,...,n
    # External vertices 0,...,1-m
    PART_EXT = 'ext'
    PART_INT = 'int'

    INT_FORMAT = PART_INT + '{}'
    EXT_FORMAT = PART_EXT + '{}'

    _GRAPH_NAME = 'Admissible'

    def __init__(self, data=None, partition=None, check=True, format=None,
                 weight=None, external_order=None, *args, **kwargs):
        # Check for loops/multiedges

        if 'loops' in kwargs and kwargs['loops']:
            raise RuntimeError("Admissible graphs cannot have loops")
        if 'multiedges' in kwargs and kwargs['multiedges']:
            raise RuntimeError("Admissible graphs cannot have multiple edges")

        if isinstance(data, GenericGraph):
            if data.has_loops():
                raise RuntimeError("Admissible graphs cannot have loops")
            if data.has_multiple_edges():
                raise RuntimeError("Admissible graphs cannot have multiple edges")

        kwargs['loops'] = False
        kwargs['multiedges'] = False

        # If no format is specified, check if we have a list of out-neighbours of internal vertices
        if format == 'out_neighbours' or (format is None and isinstance(data, list)):
            if format == 'out_neighbours' or (data and isinstance(data[0], list)):
                # Assume list of edges out of internal vertices
                internal_order = len(data)
                edges = [[KontsevichAdmissibleGraph.INT_FORMAT.format(v), w]
                         for v, out_list in enumerate(data) for w in out_list]
                if external_order is not None:
                    min_ext_order = external_order-1
                else:
                    min_ext_order = -1
                max_ext = max([min_ext_order] + [int(edge[1][3:]) for edge in edges if edge[1].startswith('ext')])
                if partition is None:
                    int_vertices = [KontsevichAdmissibleGraph.INT_FORMAT.format(i) for i in range(internal_order)]
                    ext_vertices = [KontsevichAdmissibleGraph.EXT_FORMAT.format(i) for i in range(max_ext+1)]
                    partition = {KontsevichAdmissibleGraph.PART_INT: int_vertices,
                                 KontsevichAdmissibleGraph.PART_EXT: ext_vertices}
                data = [int_vertices + ext_vertices, edges]
                format = 'vertices_and_edges'

        self._disable_check = True

        if isinstance(data, GenericGraph) and not isinstance(data, PartitionGenericGraph) and partition is None:
            # If we have a graph without a partition, assume all vertices with
            # out-degree 0 are external
            ext_vertices = set()
            int_vertices = set()
            for vertex in data.vertices():
                if data.out_degree(vertex) == 0:
                    ext_vertices.add(vertex)
                else:
                    int_vertices.add(vertex)

            partition = {KontsevichAdmissibleGraph.PART_EXT: ext_vertices,
                         KontsevichAdmissibleGraph.PART_INT: int_vertices}

        if format == 'dig6':
            data = DiGraph(data, format='dig6')
            ext_vertices = ['ext'+str(i) for i in range(external_order)]
            int_vertices = ['int'+str(i-2) for i in range(external_order, data.order())]
            vertex_map = {i: ext_vertices[i] for i in range(external_order)}
            vertex_map.update({i: int_vertices[i-2] for i in range(external_order, data.order())})
            partition = {'ext': ext_vertices, 'int': int_vertices}

            data.relabel(vertex_map)
            format = None

        # If we are trying to create an immutable graph, but need to add
        # vertices, first create a copy of this graph and add vertices to that.
        if external_order is not None:
            G = PartitionDiGraph(data, partition, check, format=format, *args, **kwargs)
            if G.is_immutable():
                G = G.copy(immutable=False)
            while len(G.partition['ext']) < external_order:
                G.add_vertex('ext')
            if hasattr(data, '_disable_check'):
                G._disable_check = data._disable_check
            data = G
            partition = None
            format = None

        PartitionDiGraph.__init__(self, data, partition, check, format=format, *args, **kwargs)

        if weight is not None and not self.is_immutable():
            raise ValueError("Cannot set cached weight for a mutable graph.")

        if weight is None and isinstance(data, KontsevichAdmissibleGraph):
            weight = data._cached_weight

        self._cached_weight = weight

        if hasattr(data, '_disable_check'):
            self._disable_check = data._disable_check
        else:
            self._disable_check = False

        if check and not self._disable_check:
            self.validate()

    def _repr_(self):
        """Return a string representation of this object.

        EXAMPLES::

            sage: print(kontsevichgraphs.Wedge(1)._repr_())
            Kontsevich graph (Quantization) on (1, 2) vertices
            sage: print(kontsevichgraphs.Wedge(2)._repr_())
            Kontsevich graph (Quantization) on (2, 2) vertices
        """
        n, m = self.internal_order(), self.external_order()
        _s = "Kontsevich graph ({}) on ({}, {}) vertices"
        return _s.format(self.__class__._GRAPH_NAME, n, m)

    def external_order(self):
        """Return the number of external vertices of this graph.

        INPUT:

        None

        OUTPUT:

        The number of internal vertices of the graph.

        EXAMPLES::

            sage: kontsevichgraphs.Wedge(4).external_order()
            2
        """
        return len(self.partition[KontsevichAdmissibleGraph.PART_EXT])

    def internal_order(self):
        """Return the number of internal vertices of this graph.

        INPUT:

        None

        OUTPUT:

        The number of internal vertices of the graph

        EXAMPLES::

            sage: kontsevichgraphs.Wedge(4).internal_order()
            4
        """
        return len(self.internal_vertices())

    def external_vertices(self):
        """Return a sorted list of external vertices.

        INPUT:

        None

        OUTPUT:

        A list of external vertices of this graph.

        EXAMPLES::

            sage: kontsevichgraphs.Wedge(4).external_vertices()
            ['ext0', 'ext1']
        """
        vertices = self.partition[KontsevichAdmissibleGraph.PART_EXT]
        try:
            vertex_numbers = {(int(v[3:]) if v.startswith('ext') else int(v)): v for v in vertices}
            return [vertex_numbers[index] for index in sorted(vertex_numbers)]
        except (AttributeError, ValueError):
            return list(vertices)

    def internal_vertices(self):
        """Return a sorted list of internal vertices.

        INPUT:

        None

        OUTPUT:

        A list of internal vertices of this graph.

        EXAMPLES::

            [sage: kontsevichgraphs.Wedge(4).internal_vertices()
            {'int0', 'int1', 'int2', 'int3'}
        """
        vertices = self.partition[KontsevichAdmissibleGraph.PART_INT]
        try:
            vertex_numbers = {(int(v[3:]) if v.startswith('int') else int(v)): v for v in vertices}
            return [vertex_numbers[index] for index in sorted(vertex_numbers)]
        except (AttributeError, ValueError):
            return list(vertices)

    def differential_form_degree(self):
        """Returns the degree of the differential form associated to this graph.

        INPUT:

        None

        OUTPUT:

        The degree of the differential form that is used to calculate the
        weight of this graph.

        EXAMPLES::

            sage: kontsevichgraphs.Bernoulli(4).differential_form_degree()
            8
        """

        return 2*self.internal_order() + self.external_order() - 2

    def validate(self):
        """Check to see if this graph is a valid admissible Kontsevich graph.

        INPUT:

        None

        OUTPUT:

        None. If the graph is invalid, an exception is raised.

        EXAMPLES:

        To generate an invalid graph, we have to specify ``check=False`` in
        the constructor::

            sage: G = KontsevichAdmissibleGraph(check=False)
            sage: G.validate()
            Traceback (most recent call last):
            ...
            RuntimeError: Invalid admissible graph: 2n+m-2 must be non-negative
        """
        if not self.differential_form_degree() >= 0:
            raise RuntimeError("Invalid admissible graph: 2n+m-2 must be non-negative")

        return self.differential_form_degree() >= 0

    def is_standard_form(self):
        """Check if this graph is in its standard form.

        Standard form means that the internal vertices are numbered from 0 to
        n-1, and the external vertices are numbered from n to n+m-1, OR
        internal vertices labeled 'int<x>', x from 0 to n-1, and external
        vertices 'ext<x>', x from 0 to m-1
        """
        valid = list(self.internal_vertices()) == range(self.internal_order())
        valid &= list(self.external_vertices()) == range(self.internal_order(), self.order())
        if valid:
            return 'integer'
        valid = self.internal_vertices() == {'int' + str(x) for x in range(self.internal_order())}
        valid &= self.external_vertices() == ['ext' + str(x) for x in range(self.external_order())]
        if valid:
            return 'string'
        return False

    def _to_std_integer(self):
        vertex_map = {s: int(s[3:]) for s in self.internal_vertices()}
        vertex_map.update({s: int(s[3:]) + self.internal_order() for s in self.external_vertices()})
        vertices = [vertex_map[v] for v in self]
        edges = [(vertex_map[u], vertex_map[v], l) for u, v, l in self.edges()]
        partition = {K: [vertex_map[v] for v in self.partition[K]] for K in self.partition}
        return self.__class__([vertices, edges], format='vertices_and_edges', partition=partition)

    def plot(self, **options):
        """Return a graphics object representing the (di)graph.

        This takes the same arguments as :meth:`GenericGraph.plot`. However,
        if the keyword argument 'partition' is not specified, default to using
        the vertex partition.

        Furthermore, a new option is available for the argument ``layout``: if
        this has value "ranked_external", then each vertex will be assigned a
        rank equal to the minimal distance between it and an external vertex.
        The vertices of rank $n$ will then be drawn on a circle with centre
        $(0, n)$ and radius $r$, where $r$ corresponds to the paramater ``r``.
        """
        if 'layout' not in options or options['layout'] == 'ranked_external':
            import math
            # We first compute the ranks for each vertex, then position
            # vertices in an arbitrary order on a circle with centre (0, rank)
            # and radius r
            r = options['r'] if 'r' in options else 0.4

            # @TODO: this should really take into account connected components
            # of (an undirected copy of) this Graph minus all external vertices
            ranks = defaultdict(list)
            for u in self.vertices():
                d = min(self.distance(u, v) for v in self.external_vertices())
                ranks[d].append(u)

            pos = dict()

            from sage.graphs.graph import Graph
            H = Graph(self)
            H.delete_vertices(self.external_vertices())

            components = H.connected_components()

            def layout_circle(vertices, centre, radius):
                n = len(vertices)
                pos = dict()
                if n == 0:
                    pass
                elif n == 1:
                    pos[vertices[0]] = (centre[0], centre[1])
                else:
                    deltatheta = 2*math.pi/len(vertices)
                    for k, v in enumerate(vertices):
                        x = centre[0] + radius*cos(deltatheta*k)
                        y = centre[1] + radius*sin(deltatheta*k)
                        pos[v] = (x, y)
                return pos

            pos = layout_circle(self.external_vertices(), (0, 0), r)
            # Set the external vertices first
            for i, component in enumerate(components):
                for rank in ranks.keys():
                    vertices = [v for v in ranks[rank] if v in component]
                    deltax = 0.75 * (2*i - len(components) + 1)
                    # alter radius so we don't have vertically stacked vertices
                    deltax = deltax
                    pos.update(layout_circle(vertices, (deltax, rank), r - 0.05*rank))

            options['pos'] = pos

        if 'vertex_size' not in options:
            options['vertex_size'] = 500

        return super(KontsevichAdmissibleGraph, self).plot(**options)

    def show(self, method='matplotlib', **kwds):
        old_disable_check = self._disable_check
        self._disable_check = True
        res = super(KontsevichAdmissibleGraph, self).show(**kwds)
        self._disable_check = old_disable_check
        return res

    @staticmethod
    def _parse_edge_args(u, v, label):
        """Parse a tuple (u, v, label) into an edge specification.

        If neither ``label`` or ``v`` is specified, we try to interpret ``u``
        as either a tuple ``(u, v, label)`` or just ``(u, v)``. If ``label`` is
        specified, but ``v`` is not, then we interpret ``u`` as a tuple
        ``(u, v)``.
        """
        # From graphs/generic_graph.py:10208
        if label is None:
            if v is None:
                try:
                    u, v, label = u
                except ValueError:
                    try:
                        u, v = u
                    except ValueError:
                        pass
        else:
            if v is None:
                try:
                    u, v = u
                except ValueError:
                    pass
        return u, v, label

    def _parse_vertex(self, part, name):
        """Parse a pair (part, name).

        If ``part`` is neither 'int' or 'ext', we interprate it as the vertex
        name. If it is ``None``, we interprate ``name`` as the vertex name.
        """
        # If part is valid, perofmr checks on name
        if part in ('int', 'ext'):
            if name is None:
                n = 0
                while part + str(n) in self:
                    n += 1
                name = part + str(n)
            return part, name

        # If part is not a partition name, it must be of the form <part><vertex>
        # Then we require name to be none
        if name is not None:
            raise RuntimeError(
                "Part must be either {} or {}, or these followed by a string".format(repr('int'), repr('ext')))

        name = part
        part = part[:3]

        if part not in ('int', 'ext'):
            raise RuntimeError(
                "Part must be either {} or {}, or these followed by a string".format(repr('int'), repr('ext')))

        try:
            part = int(part)
        except ValueError:
            pass

        return part, name

    def add_vertex(self, part, name=None, check=True):
        return_name = name is None
        part, name = self._parse_vertex(part, name)
        if check and not self._disable_check:
            self._validate_op(['add_vertices', [(part, name)]])

        super(KontsevichAdmissibleGraph, self).add_vertex(part, name)
        if return_name:
            return name

    def add_vertices(self, part, vertices=None, check=True):
        if vertices is None:
            part, vertices = zip(*[self._parse_vertex(v, None) for v in part])
        elif hasattr(part, '__iter__'):
            part, vertices = zip(*[self._parse_vertex(p, v) for (p, v) in zip(part, vertices)])
        else:
            if not (part == 'ext' or part == 'int'):
                raise RuntimeError("Part must be either {} or {}".format(repr('int'), repr('ext')))
            part = [part]*len(vertices)

        if check and not self._disable_check:
            self._validate_op(['add_vertices', zip(part, vertices)])

        return super(KontsevichAdmissibleGraph, self).add_vertices(part, vertices)

    def delete_vertex(self, vertex, in_order=False, from_partition=None, check=True):
        if in_order and from_partition is not None:
            raise RuntimeError("The arguments 'in_order' and 'from_partition' are mutually exclusive")

        if in_order:
            vertex = self.vertices()[vertex]

        if from_partition is not None:
            vertex = self.partition[from_partition][vertex]

        if check and not self._disable_check:
            self._validate_op(['delete_vertices', [vertex]])

        return super(KontsevichAdmissibleGraph, self).delete_vertex(vertex)

    def delete_vertices(self, vertices, check=True):
        if check and not self._disable_check:
            self._validate_op(['delete_vertices', vertices])

        return super(KontsevichAdmissibleGraph, self).delete_vertices(vertices)

    def add_edge(self, u, v=None, label=None, check=True):
        """

        TESTS::

            sage: G = KontsevichAdmissibleGraph(check=False)
            sage: G.add_vertices(['int0', 'ext0'])
            sage: G.add_edge('int0', 'ext0') # both exist
            sage: G.add_edge('int1', 'ext1') # neither exist
            sage: G.add_edge('int0', 'ext2') # first exists
            sage: G.add_edge('int3', 'ext0') # second exists
            sage: G.add_edge('int0', 'int4') # internal
        """
        return self.add_edges([(u, v, label)], check=check)

    def add_edges(self, edges, check=True):
        # Parse list of edges, and extract vertices to add
        edges = [[e[0], e[1]] for e in edges]
        new_parts = []
        new_vertices = []
        lowest_new_vertex = 0
        new_edges = []
        for edge in edges:
            if edge in self.edges():
                continue
            new_edges.append(edge)
            for i in range(2):
                if edge[i] not in self.vertices() + new_vertices:
                    if edge[i] is None:
                        new_parts.append('int')
                        while 'int' + str(lowest_new_vertex) in self:
                            lowest_new_vertex += 1
                        edge[i] = 'int' + str(lowest_new_vertex)
                    else:
                        new_parts.append(edge[i][:3])
                    new_vertices.append(edge[i])

        if check and not self._disable_check:
            if new_vertices:
                self._validate_op(['add_vertices', zip(new_parts, new_vertices)], ['add_edges', new_edges])
            else:
                self._validate_op(['add_edges', new_edges])

        if new_vertices:
            self.add_vertices(new_vertices, check=False)

        return super(KontsevichAdmissibleGraph, self).add_edges(new_edges)

    def delete_edge(self, u, v=None, label=None, check=True):
        if check and not self._disable_check:
            u, v, label = KontsevichAdmissibleGraph._parse_edge_args(u, v, label)
            self._validate_op(['delete_edges', [u, v, label]])

        return super(KontsevichAdmissibleGraph, self).delete_edge(u, v, label)

    def delete_edges(self, edges, check=True):
        disable_check = self._disable_check

        if check and not self._disable_check:
            self._validate_op(['delete_edges', edges])
            self._disable_check = True

        res = super(KontsevichAdmissibleGraph, self).delete_edges(edges)
        self._disable_check = disable_check
        return res

    def _validate_op(self, *args):
        """Check if a given operation is valid on this graph.

        INPUT:

        - ``*args`` - a list, with each entry a pair ``(method, method_args)``:
          - ``method`` - one of {add,delete}_{vertices,edges}
          - ``method_args`` - for ``add_vertices``, this is a list of pairs
            ``(part, name)``. For `delete_vertices``, this is a list of
            vertices. For ``add_edges`` and ``delete_edges``, this is a list of
            edges, with each entry either of the form ``(u, v)`` or
            ``(u, v, label)``

        OUTPUT:

        None. If the input does not specify a valid operation, raise a
        ``RuntimeError``.
        """
        delta = 0
        for method, method_args in args:
            if method == 'add_vertices':
                delta += sum(2 if part.startswith('int') else 1 for part, v in method_args)
            if method == 'delete_vertices':
                delta -= sum(2 if v in self.internal_vertices() else 1 for v in method_args)

        if self.differential_form_degree() + delta < 0:
            raise RuntimeError("Deleting vertices breaks admissibile graph condition")

    def subgraph(self, *args, **kwargs):
        old_disable_check = self._disable_check
        self._disable_check = True
        subgraph = super(KontsevichAdmissibleGraph, self).subgraph(*args, **kwargs)
        self._disable_check = old_disable_check
        return subgraph

    def diffop(self, P, f, g, do_sum=True):
        """Calculate the differential operator for this graph.

        This is not implemented for admissible graphs, and will remain as such
        until after polyvectors are implemented in Sage (:trac:`18528`)
        """
        raise NotImplementedError

    def isomorphism_class_size(self):
        """Calculate the number of graphs isomorphic to self.

        INPUT:

        None

        OUTPUT:

        The number of graphs which are isomorphic to this one.

        We consider graphs with:

        - Labels on vertices (internal and external)
        - Orderings on the set of edges out of each (internal) vertex

        We require external vertices to be mapped to external vertices.

        ALGORITHM:

        We compute this value by dividing the total number of labellings of
        this graph by the number of automorphisms of the graph. The total
        number of labellings is given by n! * m! (as we label internal and
        external vertices independently). There is then a factor of
        out_degree(v)! for every vertex v (for the orderings of the edges out
        of this vertex).

        Finally, the number of automorphisms is computed using
        meth:`automorphism_group`.
        """

        n, m = self.internal_order(), self.external_order()
        aut_group_order = self.automorphism_group(
            order=True, return_group=False, partition=self.partition_list())
        edge_orders = prod([factorial(k) for k in self.out_degree() if k != 0])
        return (edge_orders * factorial(m) * factorial(n)) / aut_group_order

    def canonical_label(self, partition=None, certificate=None,
                        edge_labels=False, algorithm=None, return_graph=True):
        """Return a canonical form of this graph."""

        res = super(KontsevichAdmissibleGraph, self).canonical_label(partition, True, 
                                                                     edge_labels, algorithm,
                                                                     return_graph)
        G, canonical_map = res

        # We relabel the numbers back to 'int<x>', 'ext<y>'
        current_vertices = {'int': self.internal_vertices(), 'ext': self.external_vertices()}

        vertex_map = dict()
        for part in current_vertices:
            new_labels = [canonical_map[v] for v in current_vertices[part]]
            sorted_labels = sorted(new_labels)
            vertex_map.update({l: part+str(sorted_labels.index(l)) for l in new_labels})

        G.relabel(vertex_map)
        # G is mutable, so clear its cached weight
        G._cached_weight = None
        if not certificate:
            if return_graph:
                return G
            else:
                return None

        canonical_map = {v: vertex_map[canonical_map[v]] for v in self.vertices()}
        if return_graph:
            return G, canonical_map
        return canonical_map

    ###################
    # Maple functions #
    ###################
    #
    # The actual weight integration routine is written in Maple.
    #
    # The format of graphs there are as follows:
    # - external vertices are labelled p1, ..., pm
    # - internal vertices are labelled 1, ..., n
    # - a graph is represented by a list of out-neighbours of vertices
    #
    # An exception is for a graph which has exactly two external vertices, in
    # which case the external vertices are labelled "L" and "R" respectively.
    #
    # For example, the Bernoulli graph on two internal vertices is represented
    # by
    #
    #     [[2,p2],[p1,p2]]
    #

    def _to_maple(self):
        vertex_dict = dict()

        if len(self.external_vertices()) == 2:
            vertex_dict[self.external_vertices()[0]] = 'L'
            vertex_dict[self.external_vertices()[1]] = 'R'
        else:
            for n, l in enumerate(self.external_vertices()):
                vertex_dict[l] = 'p' + str(n+1)

        for n, l in enumerate(self.internal_vertices()):
            vertex_dict[l] = n+1

        neighbours_list = [self.neighbors_out(v) for v in self.internal_vertices()]
        neighbours_maple = [[vertex_dict[v] for v in sorted(L)] for L in neighbours_list]

        return str(neighbours_maple).replace("'", "")

    @staticmethod
    def _from_maple(input_str, calc_sign=False):
        """Convert a list of edges in the form the Maple code uses to a graph.

        EXAMPLES::

        TODO update this doctest::

            sage: G = KontsevichAdmissibleGraph._from_maple('[[p1, 2], [p1, 3], [p1, p2]]')
            sage: G.is_isomorphic(kontsevichgraphs.Bernoulli(3))
            True
            sage: G = kontsevichgraphs.Wedge(3)
            sage: H = KontsevichAdmissibleGraph._from_maple(G._to_maple())
            sage: H == G
            True
        """
        def number_to_vertex(i):
            if i < 2:
                return 'ext' + str(i)
            else:
                return 'int' + str(i-2)

        neighbours_list = eval(input_str, {'L': -1, 'R': 0, 'p1': -1, 'p2': 0})
        neighbours_sage = [[number_to_vertex(v+1) for v in L] for L in neighbours_list]
        G = KontsevichAdmissibleGraph(neighbours_sage, format='out_neighbours')

        if calc_sign:
            # We assume that edges are orderder by destination vertex; that is,
            # an edge to a lower-index vertex comes before a higher-index
            # vertex. Hence we have alterned the sign by (-1)**n, where n is
            # the number of times we have exchanged edges. This is a sum over
            # all vertices of the number of transpositions needed to return the
            # neighbours_list to being sorted.
            n = 0
            for L in neighbours_list:
                if len(L) < 2:
                    pass
                elif len(L) == 2:
                    if L[0] > L[1]:
                        n = n+1
                else:
                    L_sorted = sorted(L)
                    L_permutation = [L_sorted.index(v)+1 for v in L]
                    if not Permutation(L_permutation).is_even():
                        n = n+1
            sign = 1 if n % 2 == 0 else -1
            return G, sign
        return G

    @staticmethod
    def _parse_maple(maple_string):
        """Convert a string representing a Maple expression into a symbolic
        expression.

        This currently can handle strings containing the constant Pi and any
        Zeta values.
        """
        import re
        maple_string = maple_string.replace('Pi', 'pi')
        maple_string = maple_string.replace('Zeta', 'zeta')
        return SR(re.sub(r'zeta\[([\d]*?)\]', r'zeta(\1)', maple_string))


    # @TODO: implement the following (in the relevant classes):
    # product, is_kontsevich_product, product_weight
    # diffop_single, _labellings_raw, _labellings, diffop, diffop_full
    # weight_vanishes
    # weight_integrand, calc_weight, weight

class KontsevichFormalityGraph(KontsevichAdmissibleGraph):
    _GRAPH_NAME = "Formality"

    def weight_integrand(self):
        """Calculate the integrand associated to this graph.

        INPUT:

        - ``varname`` -- (default `z`) a name for the coordinate variables
        of the integrand, as a string

        OUTPUT:

        The integrand used to calculate the weight of this graph,
        expressed in coordinates $z_i,zb_i$ for $i$ from $0$ to
        $n-1$, where $n$ is the number of internal vertices.

        We also return these variables as a list.

        NOTE:

        We do not include the factor of $(2*pi)^{2n}$, nor the factor of
        $1/n!$.

        EXAMPLES::

        @TODO fix weight_integrand::

            # sage: W, varlist = kontsevichgraphs.list(1)[0].weight_integrand() 
            # sage: W
            # (z0 - zb0)/(z0^2*zb0^2 - z0^2*zb0 - z0*zb0^2 + z0*zb0)
            # sage: varlist
            # (z0, zb0)
        """
        raise NotImplementedError

    def weight_vanishes(self):
        # Due to Panzer - if there is an isolated external vertex, then the
        # corresponding term dz never appears in the volume form, so it must
        # vanish
        if self.internal_order() >= 1:
            if min([self.degree(v) for v in self.external_vertices()]) < 1:
                return True

        # Due to Panzer - if there is an internal vertex with fewer than two
        # incoming edges, we can only find either a dz or dzbar in the volume
        # form, but never both
        if self.internal_order() >= 2:
            if min([self.degree(v) for v in self.internal_vertices()]) < 2:
                return True

        return False

    def weight_unitary(self):
        # With no internal vertices, the weight becomes an empty product
        if self.internal_order() == 0:
            return True
        return False

    def weight_calc(self):
        """Calculate the weight of this graph, excluding the prefactor.

        This uses Erik Panzer's `kontsevint` integration routine, written in
        Maple.

        ALGORITHM:

        The ``kontsevint`` library provides two integration routines: one
        which is optimized for quantization graphs, and one which can handle
        all admissible graphs. We restart Maple if neccessary to load the
        correct routine.
        """
        from sage.interfaces.maple import maple
        import re
        maple_type_var = '_kontsevichgraphs_type'

        if self.external_order() == 2:
            self_type = 'kontsv01.mpl'
        else:
            self_type = 'kontsv.mpl'

        maple_type = maple.get(maple_type_var)
        if maple_type != self_type:
            maple.quit()
            maple.eval('d := currentdir("{}"); read("{}"); currentdir(d)'.format(MAPLE_DIR, self_type))
            maple.set(maple_type_var, self_type)

        weight_str = str(maple.weight(self._to_maple()))
        weight_str = weight_str.replace('Pi', 'pi')
        weight = SR(re.sub(r'zeta\[([\d]*?)\]', r'zeta(\1)', weight_str))
        return weight

    def weight(self):
        """Return the weight of this graph."""
        if self.weight_vanishes():
            return 0

        if self.weight_unitary():
            return 1

        if self.is_immutable() and self._cached_weight is not None:
            return self._cached_weight

        from .kontsevichgraph_generators import kontsevichgraphs
        weight = kontsevichgraphs.lookup_weight(self, False)

        if weight is None:
            weight = self.weight_calc()

        if self.is_immutable():
            self._cached_weight = weight

        return weight

    def isomorphism_weight_effect(self, iso):
        raise NotImplementedError

    def _validate_op(self, *fnargs):
        delta = 0
        for method, args in fnargs:
            if method == 'add_vertices':
                delta += sum(2 if part == 'int' else 1 for part, vertex in args)
            if method == 'delete_vertices':
                vertices = [v for p, v in args]
                delta -= sum(2 if vertex in self.internal_vertices() else 1 for vertex in vertices)
                # All edges from v in vertices   to somewhere else
                edges_out = set(self.edge_boundary(vertices, self.vertices()))
                # All edges to   v in vertices from somewhere else
                edges_in = set(self.edge_boundary(self.vertices(), vertices))
                edges = edges_out.union(edges_in)
                delta += len(edges)
            if method == 'add_edges':
                delta -= len(args)
            if method == 'delete_edges':
                delta += len(args)

        if self.differential_form_degree() - delta != self.size():
            raise RuntimeError("Invalid operation: breaks formality graph condition")
        super(KontsevichFormalityGraph, self)._validate_op(*fnargs)

    def subdivide_edge(self, check=True, *args):
        if check and not self._disable_check:
            raise RuntimeError("Subdividing edge breaks formality graph condition")
        return super(KontsevichFormalityGraph, self).subdivide_edge(*args)

    def subdivide_edges(self, edges, k, check=True):
        if check and not self._disable_check:
            raise RuntimeError("Subdividing edges breaks formality graph condition")
        return super(KontsevichFormalityGraph, self).subdivide_edges(edges, k)

    def validate(self):
        super(KontsevichFormalityGraph, self).validate()
        if not self.size() == self.differential_form_degree():
            raise RuntimeError("Invalid formality graph: 2n+m-2 must equal the number of edges")

class KontsevichQuantizationGraph(KontsevichFormalityGraph):
    _GRAPH_NAME = "Quantization"

    def __init__(self, data=None, partition=None, check=True, format=None,
                 weight=None, external_order=None, *args, **kwargs):
        # If we are given no information, create a graph on two external vertices
        if data is None and partition is None:
            data = [['ext0', 'ext1'], []]
            format = 'vertices_and_edges'
            partition = {'int': {}, 'ext': {'ext0', 'ext1'}}

        super(KontsevichQuantizationGraph, self).__init__(data, partition, check, format,
                                                          weight, external_order=2, *args, **kwargs)

    def are_external_vertices_fixed(self):
        """Whether there is an automorphism of the graph which permutes the
        external vertices.

        INPUT:

        None

        OUTPUT:

        If there is an automorphism mapping L to R, return ``True``. Otherwise
        return ``False``.

        NOTES:

        If the graph has an automorphism mapping L to R, then we do not need to
        split the graph up when calculating the star product. However, if there
        is no such automorphism, then when we calculate the differential
        operator we have to consider two cases: one with L and R as they are,
        and another with L and R flipped. Each of these are isomorphic (in the
        sense of fixing the external vertices) to half of the graphs which we
        have counted in :meth:`iso_class_size`. In this case, there are two
        contributions from the graph: one given by ``diffop(P, f, g)`` and one
        by ``diffop(P, g, f)``.

        ALGORITHM:

        We generate the orbits of the automorphisms of the graph, and check to
        see if [0] is an orbit. In this case, the external vertices must be
        fixed.

        EXAMPLES:

        Check whether the vertices are fixed for small graphs::

            sage: kontsevichgraphs.Wedge(3).are_external_vertices_fixed()
            False
            sage: kontsevichgraphs.Bernoulli(3).are_external_vertices_fixed()
            True

        .. SEEALSO::

          :meth:`diffop_full`
        """
        orbits = self.automorphism_group(partition=self.partition_list(), return_group=False, orbits=True)
        ext_vertex_0 = list(self.external_vertices())[0]
        return orbits.count([ext_vertex_0]) == 1

    def flip_factor(self):
        r"""Calculate the flip factor: the change in sign of the weight under swapping L and R

        INPUT:

        None

        OUTPUT:

        The change in sign of the weight of this graph after swapping L and R

        ALGORITHM:

        This value is $(-1)^n$, where $n$ is the internal order. This follows
        by applying the map $p \mapsto 1-\conj{p}$, which swaps 0 and 1 and
        leaves the rest of the integrand unchanged. As it is orientation-reversing,
        this introduces a factor of $(-1)^n$.

        Note that it also introduces a conjugate: in general, the weight of the
        flipped graph is $G.flip_factor() * conjugate(G.weight())$.

        EXAMPLES::

            sage: G = kontsevichgraphs.Bernoulli(3)
            sage: G.flip_factor()
            -1

        .. SEEALSO::

          :meth:`isomorphism_sign`
        """
        return (-1)**self.internal_order()

    def isomorphism_weight_effect(self, iso):
        r"""Calcuate the effect on the weight of this graph of applying a given
        isomorphism.

        INPUT:

        - ``iso`` -- the isomorphism to apply, either in the form of a
          dictionary or a function

        OUTPUT:

        A function mapping the weight of this graph to the weight of
        the graph after applying the isomorphism.

        ALGORITHM:

        Suppose the ismorphism fixes the external vertices; then the only way
        that the weight can change is if the order a pair of edges is flipped.
        The edges are implicitly ordered by the number of the vertex they go
        to, so if $v$ has edges to $w_1$ and $w_2$, the edge $vw_1$ is
        considered the first edge iff $w_1 < w_2$. As such, if a vertex $v$ is
        mapped to $v'$, with $w_i$ mapped to $w_i'$, the sign changes iff
        $(w_1 - w_2)*(w_1'-w_2') < 0$.

        Suppose instead that the isomorphism flips the external vertices.
        Initially say this is the only change. Then we can transform one weight
        integral into the other by appyling the orientation-reversing map of
        the upper half-plane $z \mapsto 1-\conj{z}$. This reflection maps $0$
        to $1$, but leaves the angle function unchanged. Thus we have that the
        effect on the sign is a factor of $(-1)^n$, where $n$ is the order of
        the graph, plus a conjugation of the weight.

        Finally, these two can be combined. Let $r$ be the number of edges
        flipped excluding wedges between L and R, and $n$ the internal order.
        Then:

        - if the isomorphism fixes L and R, then the sign change is $(-1)^r$
        - if the isomorphism swaps L and R, then the sign change is $(-1)^n+r$

        EXAMPLES:

        Caclulate the sign change for an isomorphism fixing L and R
        ::

            sage: G = KontsevichQuantizationGraph([['ext0', 'int1'], ['int0', 'ext1'], ['int0', 'int1']])
            sage: iso = {2: 3, 3: 2}
            sage: H = G.relabel(iso, inplace=False)
            sage: bool(G.isomorphism_weight_effect(iso)(G.weight()) == H.weight())
            True

        Caculate the sign change for an isomorphism flipping L and R only
        ::

            sage: G = kontsevichgraphs.Chain(2)
            sage: iso = {'ext0': 'ext1', 'ext1': 'ext0'}
            sage: H = G.relabel(iso, inplace=False)
            sage: bool(G.isomorphism_weight_effect(iso)(G.weight()) == H.weight())
            True

        Generate a Kontsevich graph, apply a random isomorphism and check the
        answer is correct
        @TODO this doctest is currently broken (the underlying feature is not working)
        ::

            sage: from sage.groups.perm_gps.permgroup import PermutationGroup
            sage: S4 = PermutationGroup([('ext0', 'ext1'), ('int0', 'int1', 'int2', 'int3'), ('int0', 'int1')])
            sage: is_zero_weight = True
            sage: while is_zero_weight:
            ....:     G = kontsevichgraphs.Random(4)
            ....:     is_zero_weight = G.weight() == 0
            sage: iso = S4.random_element()
            sage: H = G.relabel(iso.dict(), inplace=False)
            sage: bool(G.isomorphism_weight_effect(iso)(G.weight()) == H.weight()) # optional - maple
            True

        The isomorphism must send external vertices to external vertices::
            sage: G = kontsevichgraphs.Wedge(3)
            sage: iso = {'ext0': 'int0', 'int0': 'ext0'}
            sage: G.isomorphism_weight_effect(iso)
            Traceback (most recent call last):
            ...
            ValueError: The isomorphism must map external vertices to external vertices
        """
        self.validate()

        if isinstance(iso, dict):
            vertex_map = lambda k: iso[k] if k in iso.keys() else k
        else:
            vertex_map = iso

        if {vertex_map(v) for v in self.external_vertices()} != set(self.external_vertices()):
            raise ValueError(
                "The isomorphism must map external vertices to external vertices")

        # Calculate the number of flipped edge orders
        partition_offset = lambda s: self.external_order() if s == 'int' else 0
        vertex_index = lambda s: int(s[3:]) + partition_offset(s[:3]) if isinstance(s, str) else s
        sign = 1
        internal_neighbours = [self.neighbors_out(v) for v in self.internal_vertices()]
        sign = prod(sgn((vertex_index(y) - vertex_index(x))*(vertex_index(vertex_map(y))-vertex_index(vertex_map(x)))) for x, y in internal_neighbours)

        do_conjugate = False
        ext_vertex_0, ext_vertex_1 = self.external_vertices()
        # If we flip L and R, multiply by (-1)^n and conjugate
        if vertex_map(ext_vertex_0) == ext_vertex_1 and self.internal_order() % 2 == 1:
            sign *= -1
            do_conjugate = True

        if do_conjugate:
            return lambda w: sign * conjugate(w)

        return lambda w: sign * w

    def diffop_single(self, P, f, g, out_labels, in_labels):
        """Calculate the Kontsevich differential operator associated with the
        given labelling of this graph.

        INPUT:

        - ``P`` -- the Poisson bracket to deform
        - ``f`` -- the first function to apply the operator to
        - ``g`` -- the second function to apply the operator to
        - ``labels`` -- the set of labels to use, in the form of a dictionary.
          The value ``labels[v1, v2]`` is the label on edge the edge between v1
          and v2.

        OUTPUT:

        An expression involving the derivatives of the entries of the Poisson
        tensor specified by ``P`` and derivatives of ``f`` and ``g``. See
        :meth:`diffop_full` for more details.

        This function is normally called by :meth:`diffop_full`, and so the
        user doesn't have to worry about calling it directly.

        EXAMPLES:

        Apply a labeling to a graph, then calculate the differential operator
        based on that labelling.
        ::

            sage: G = kontsevichgraphs.Bernoulli(2)
            sage: P = poisson_brackets.SkewBracket()
            sage: x, y = P.vars
            sage: f = function('f')(*P.vars)
            sage: g = function('g')(*P.vars)
            sage: out_labels = {'ext0': [], 'ext1': [], 'int0': [1, 0], 'int1': [0, 1]}
            sage: in_labels = {'ext0': [1, 0], 'ext1': [1], 'int0': [], 'int1': [0]}
            sage: G.diffop_single(P, f, g, out_labels, in_labels) # long time
            -1/4*t^2*x*y^2*diff(f(x, y), x, y)*diff(g(x, y), y)
        """
        terms = {}

        # Calculate the terms involving external vertices
        external_vertices = list(self.external_vertices())
        terms[external_vertices[0]] = P.derivative(f, in_labels[external_vertices[0]])
        if terms[external_vertices[0]] == 0:
            return 0

        terms[external_vertices[1]] = P.derivative(g, in_labels[external_vertices[1]])
        if terms[external_vertices[1]] == 0:
            return 0

        # Calculate the terms involving internal vertices
        zero = False
        for v in self.internal_vertices():
            index = out_labels[v]

            terms[v] = P.derivative(
                P.poisson_matrix[index[0], index[1]], in_labels[v])
            if terms[v] == 0:
                # No point carrying on
                zero = True
                break

        if zero:
            return 0
        return prod(terms.values())

    _labellings_raw_cache = {}
    @staticmethod
    def _labellings_raw(n, d):
        """Return the labellings of n pairs of edges with numbers from 0 to d-1."""
        if (n, d) not in KontsevichQuantizationGraph._labellings_raw_cache:
            def _make_labelling(i):
                out_labels = [[]] * n

                for v in range(n):
                    x = int((i / base**(v))) % base
                    l1 = x % d
                    l2 = int(x / d)
                    if l2 >= l1:
                        l2 += 1
                    out_labels[v] = [l1, l2]
                return out_labels

            base = d * (d-1)
            KontsevichQuantizationGraph._labellings_raw_cache[n, d] = [_make_labelling(i) for i in range(base**n)]

        return KontsevichQuantizationGraph._labellings_raw_cache[n, d]

    def _labellings(self, d):
        # We have a choice of d(d-1) labellings for each vertex, giving
        # a total of (d(d-1))**n total labellings
        external_vertices = list(self.external_vertices())
        internal_vertices = list(self.internal_vertices())
        vertices = self.vertices()

        # For each edge, record its source vertex as incoming from its target
        in_labels_source = defaultdict(list)
        for v in internal_vertices:
            for i in range(2):
                # @TODO expand sorted to do proper comparisions
                in_labels_source[sorted(self.neighbors_out(v))[i]].append((v, i))

        for out_labels_raw in KontsevichQuantizationGraph._labellings_raw(self.internal_order(), d):
            out_labels = {v: [] for v in external_vertices}
            out_labels.update({v: out_labels_raw[internal_vertices.index(v)] for v in internal_vertices})
            in_labels = {v: [out_labels[w][l] for w, l in in_labels_source[v]] for v in vertices}

            yield out_labels, in_labels


    def diffop_symbolic(self):
        """returns a string with the latex symbolic sum of the bidifferential operator
        To get the latex string, use print(G.diffop_symb())"""
        cntr = 0
        labels = ['i','j','k','l','m','n','a','b','c','d','e','h','p','q','r','s','t','u','v','w','o']
        if len(self.edges()) > len(labels):
            raise Exception("Not enough labels!")

        # setting edge labels 
        for u,v,l in self.edges(sort=False):
            self.set_edge_label(u, v, labels[cntr])
            cntr += 1
        
        diffop = "\\sum_{" + ",".join(labels[:len(self.edges())]) + "}"
        # setting vertices labels
        vertices_labels = {'ext0': 'f', 'ext1': 'g'}
        for v in self.internal_vertices():
            vertices_labels[v] = [self.edge_label(v,u) for u in self.neighbors_out(v)]
            vertices_labels[v] = ' \\' + "pi^{" + vertices_labels[v][0] + vertices_labels[v][1] + '}'

        self.set_vertices(vertices_labels)

        for v in self.internal_vertices():
            for u in self.neighbors_in(v):
                diffop += ' \\' + "partial_{" + self.edge_label(u,v) + '}'
            diffop += self.get_vertex(v)

        for v in self.external_vertices():
            for u in self.neighbors_in(v):
                diffop += ' \\' + "partial_{" + self.edge_label(u,v) + '}'
            diffop += self.get_vertex(v)

        return diffop

                

    def diffop(self, P, f, g, do_sum=True):
        """Calculate the sum of Kontsevich differential operators for all valid
        labellings of this graph.

        INPUT:

        - ``P`` -- A Poisson bracket
        - ``f``, ``g`` -- function to apply the operators to
        - ``do_sum`` -- (default True) whether to sum all operators or return
          them as a list

        OUTPUT:

        The sum of differential operators for all possible edge labellings of
        this graph, applied to ``f`` and ``g``.

        EXAMPLES:
        ::

            sage: P = poisson_brackets.SklyaninBracket() # Could be any bracket
            sage: G = kontsevichgraphs.Wedge(1)
            sage: f = function('f')(*P.vars)
            sage: g = function('g')(*P.vars)
            sage: bool(G.weight() * G.isomorphism_class_size() * G.diffop(P, f, g) == P.bracket(f, g))
            True
        """
        self.validate()

        # We loop over all possible labeling of this graph and sum the
        # differential operator generated by each labeling.

        terms = (self.diffop_single(P, f, g, L[0], L[1]) for L in self._labellings(P.dimension()))
        if do_sum:
            return sum(list(terms))
        return list(terms)

    def diffop_full(self, P, f, g):
        """Calculate a differential operator for this graph and its flipped
        version if needed

        INPUT:

        - ``P`` -- the Poisson bracket to calculate the operator for
        - ``f``, ``g`` -- functions to apply the operator to

        OUTPUT:

        The differential operator for this graph and its flipped version if
        needed.

        If we consider graphs up to any isomorphism, this will combine some
        graphs which have a different differential operator by virtue of having
        different edges entering in to L and R; for example, the Bernoulli-2
        graph. To account for this, when calculating the differential operator
        we have to work out if a graph could be isomorphic to one which has a
        different neighbourhood for L and R.

        It can be seen that this only happens if there is no automorphism of
        the graph which swaps L and R. This is checked using :meth:`needs_flip`.

        If the graph does not need flipping, we include a factor of 2 with the
        differential operator. This means ``1/2 * G.diffop_full(P, f, g) *
        G.weight() * G.iso_class_size()`` gives the correct term for the
        Kontsevich star product.

        EXAMPLES:

        A graph that does not need splitting:
        ::

            sage: P = poisson_brackets.SkewBracket()
            sage: x, y = P.vars
            sage: G = kontsevichgraphs.Wedge(2)
            sage: G.are_external_vertices_fixed()
            False
            sage: G.diffop_full(P, x**2, y**2)
            2*t^2*x^2*y^2

        A graph that does need splitting:
        ::

            sage: P = poisson_brackets.SkewBracket()
            sage: x, y = P.vars
            sage: G = kontsevichgraphs.Bernoulli(2)
            sage: G.are_external_vertices_fixed()
            True
            sage: G.diffop_full(P, x**2, y**2)
            2*t^2*x^2*y^2

        .. SEEALSO::

            :meth:`are_external_vertices_fixed`
        """

        if self.are_external_vertices_fixed():
            return self.diffop(P, f, g) + self.flip_factor() * self.diffop(P, g, f)
        return 2 * self.diffop(P, f, g)

    def weight_vanishes(self):
        # The weight of the graph with no internal vertices doesn't vanish
        if self.internal_order() == 0:
            return False

        # If the graph isn't conected, we certainly have zero weight
        res = not self.is_connected()

        # If there is a 2-cycle with both vertices connected to the same
        # external vertex, we have a zero integrand
        if not res:
            for external_vertex in self.external_vertices():
                if self.in_degree(external_vertex) < 2:
                    continue
                for v, w in Combinations(self.neighbors_in(external_vertex), 2):
                    if self.has_edge(v, w) and self.has_edge(w, v):
                        res = True
                        break
                if res:
                    break

        # Generate the automorphism group of this graph which fixes itself, and
        # check all of these leave the sign unchanged.
        partition = [list(self.internal_vertices())] + [[v] for v in self.external_vertices()]
        aut_group = self.automorphism_group(partition=partition)
        for A in aut_group:
            # flips sign, no conjugation
            if self.isomorphism_weight_effect(A)(1 + I) == -1 + I:
                res = True
                break

        return res

    def _validate_op(self, *fnargs):
        delta = 0
        for method, args in fnargs:
            if method == 'add_vertices':
                delta += len([v for (part, v) in args if part == 'ext' and v not in self])
            if method == 'delete_vertices':
                delta -= [self.find_part(v) for v in args].count(KontsevichAdmissibleGraph.PART_EXT)

        if self.external_order() + delta != 2:
            raise RuntimeError("Invalid operation: break quantization graph condition")
        super(KontsevichQuantizationGraph, self)._validate_op(*fnargs)

    def validate(self):
        super(KontsevichQuantizationGraph, self).validate()
        if not self.external_order() == 2:
            raise RuntimeError("Invalid quantization graph: must have external_order == 2")

        if not self.out_degree().count(2) == self.internal_order():
            raise RuntimeError("Invalid quantization graph: all internal vertices must have out degree 2")

class KontsevichRelationGraph(KontsevichAdmissibleGraph):
    _GRAPH_NAME = "Relation"

    def _validate_op(self, *fnargs):
        delta = 0
        for method, args in fnargs:
            if method == 'add_vertices':
                delta += sum(2 if part.startswith('int') else 1 for (part, vertex) in args)
            if method == 'delete_vertices':
                delta -= sum(2 if vertex in self.internal_vertices() else 1 for vertex in args)
                # All edges from v in vertices   to somewhere else
                edges_out = set(self.edge_boundary(args, self.vertices()))
                # All edges to   v in vertices from somewhere else
                edges_in = set(self.edge_boundary(self.vertices(), args))
                edges = edges_out.union(edges_in)
                delta += len(edges)
            if method == 'add_edges':
                delta -= len(args)
            if method == 'delete_edges':
                delta += len(args)

        if self.differential_form_degree() - 1 + delta != self.size():
            raise RuntimeError("Invalid operation: breaks formality graph condition")
        super(KontsevichRelationGraph, self)._validate_op(*fnargs)

    def validate(self):
        super(KontsevichRelationGraph, self).validate()
        if not self.differential_form_degree()-1 == self.size():
            raise RuntimeError("Invalid relation graph: must have 2n+m-3 edges")

    def contract_edge(self, u, v):
        """Return a new graph formed by contracting along an edge.

        We require there to be an edge u, v for this operation to be valid.
        """
        if not self.has_edge(u, v):
            raise RuntimeError("Can only merge vertices joined by an edge.")

        if self.has_edge(v, u):
            raise RuntimeError("Cannot create loop by merging edge.")

        G = KontsevichFormalityGraph(self, immutable=False, check=False)

        current_targets = set(G.neighbors_out(u))
        new_targets = set(G.neighbors_out(v))
        if new_targets & current_targets != set():
            raise RuntimeError("Contracting along edge would create multiedge")

        current_sources = set(G.neighbors_in(u))
        new_sources = set(G.neighbors_in(v))
        if new_sources & current_sources != set():
            raise RuntimeError("Contracting along edge would create multiedge")

        G.add_edges([(e[0], u, e[2]) for e in G.incoming_edge_iterator(v) if e[0] != u], check=False)
        G.add_edges([(u, e[1], e[2]) for e in G.outgoing_edge_iterator(v)], check=False)
        G.delete_vertex(v, check=False)

        G.validate()
        G = G.canonical_label()
        if self.is_immutable():
            return G.copy(immutable=True)

        return G

    def collapse_vertices(self, vertices, target_index):
        """Collapse a set of vertices to an external vertex."""
        # @TODO correct the edge ordering here
        vertices = set(vertices)
        S1 = vertices & self.internal_vertices()

        if self.edge_boundary(vertices):
            raise RuntimeError("Cannot collapse vertices with non-trivial edge boundary")

        G = KontsevichFormalityGraph(self, check=False, immutable=False)
        # Add a new (numbered) vertex
        new_vertex = 0
        while new_vertex in G.vertices():
            new_vertex += 1
        G.add_vertex(KontsevichAdmissibleGraph.PART_EXT, name=new_vertex, check=False)
        edges_in = G.edge_boundary(set.difference(set(G.vertices()), vertices))
        new_sources = [e[0] for e in edges_in]
        for u in new_sources:
            if new_sources.count(u) > 1:
                raise RuntimeError("Collapsing vertices would create multiedge")
        G.add_edges([(e[0], new_vertex, e[2]) for e in edges_in], check=False)
        G.delete_vertices(list(vertices), check=False)
        if target_index != -1:
            G.relabel({new_vertex: target_index + self.internal_order()})
        else:
            G.relabel({new_vertex: self.order() + 1})

        G.validate()
        vertex_map = {v: i for i, v in enumerate(G.vertices())}
        G.relabel(vertex_map)
        assert(G.is_standard_form() == 'integer')

        if self.is_immutable():
            return G.copy(immutable=True)

        return G


    def weight_relation(self, return_latex=False):
        """Calculate the relations for the weights.

        For this to be possible, we require the graph to have:

         - ``G.internal_vertices() == range(G.internal_order())``
         - ``G.external_vertices() == range(G.internal_order(), G.order())``
        """
        form = self.is_standard_form()
        if form is False:
            raise RuntimeError("Calculating weight relations requires graph in standard form")

        if form == 'string':
            return self._to_std_integer().weight_relation(return_latex)

        graphs = []

        int_vertices = self.internal_vertices()
        degrees = [self.out_degree(v) for v in int_vertices]

        for u, v, label in self.edge_boundary(int_vertices, int_vertices):
            if self.has_edge(v, u):
                continue
            try:
                G = self.contract_edge(u, v)
            except RuntimeError as e:
                print(e)
                continue
            # Calculate sign according to Arnal2002
            factor = Rational((1, sum([self.out_degree(w) for w in int_vertices])))
            L = list(range(1, self.internal_order()+1))
            L.remove(u+1)
            L.remove(v+1)
            L = [u+1, v+1] + L
            coefficient = -koszul_sign(L, degrees) * factor

            graphs.append((coefficient, G))

        # Generate all sets of possible subgraphs exhaustively
        seen = dict()
        # Don't consider the case of all vertices
        seen[frozenset(self.vertices())] = True
        for vertices in Combinations(self.vertices()):
            if vertices == []:
                continue

            vertices = set(vertices)
            boundary = self.edge_boundary(vertices)
            while boundary: # while boundary not empty
                vertices = vertices.union(set([w for (u, w, l) in boundary]))
                boundary = self.edge_boundary(vertices)
            if frozenset(vertices) in seen:
                continue

            seen[frozenset(vertices)] = True

            print("Considering {}".format(vertices))
            S1 = vertices & self.internal_vertices()
            S2 = vertices & set(self.external_vertices())

            n2 = len(S1)
            m2 = len(S2)
            if m2 == 0:
                # If we don't collapse to a fixed external notice, we can insert
                # this new vertex anywhere in the new graph. Use -1 to indicate
                # this.
                l = -1
            else:
                # Any external vertices must be adjacent to preserve the
                # ordering of external vertices
                l = min(S2)
                if S2 != set(range(l, l+m2)):
                    continue
                l -= self.internal_order()


            if 2*n2 + m2-2 < 0 or n2 + m2 > self.order() - 1:
                continue

            edges = self.edge_boundary(vertices, vertices)
            if len(self.edge_boundary(vertices, vertices)) != 2*n2+m2-2:
                continue

            print("Collapsing {}".format(vertices))
            try:
                G2 = KontsevichFormalityGraph(self.collapse_vertices(vertices, l))
            except RuntimeError as e:
                print(e)
                continue
            partition = {KontsevichAdmissibleGraph.PART_INT: S1, KontsevichAdmissibleGraph.PART_EXT: S2}
            G1 = KontsevichFormalityGraph([vertices, edges], format='vertices_and_edges', partition=partition)
            G1 = G1.canonical_label()

            # Calculate sign according to Arnal2002
            I = S1
            J = set(self.internal_vertices()).difference(I)
            sigma = [x+1 for x in sorted(J) + sorted(I)]

            m_1 = m2

            k_I = sum([self.out_degree(i) for i in I])
            k_J = sum([self.out_degree(j) for j in J])
            factor = Rational((1, binomial(k_I+k_J, k_I)))
            print(k_J, k_I, l, m_1)
            print(sigma)
            print(factor)
            print(koszul_sign(sigma, degrees))
            coefficient = koszul_sign(sigma, degrees)*factor

            if l != -1:
                power = (k_J-1)*k_I + l*(m_1-1)

                if power % 2 != 0:
                    coefficient = -coefficient

                graphs.append((coefficient, G1, G2.canonical_label()))
                print('I = {}, J = {} has coefficient {}'.format(I, J, coefficient))
            else:
                source_vertex = G2.order() - 1
                vertices_to_new_vertex = set(G2.neighbors_in(source_vertex))
                vertices_with_swapped_edges = set()
                coefficient *= Rational((1, G2.external_order()))
                for l in range(G2.external_order()):
                    # reverse order to make keep track of swapped edges easier
                    target_vertex = G2.order()-1 - l
                    H = G2.copy()
                    vertex_map = {source_vertex: target_vertex}
                    vertex_map.update({n: n+1 for n in range(target_vertex, G2.order()-1)})
                    H.relabel(vertex_map)

                    power = (k_J-1)*k_I + l*(m_1-1)
                    # We also need to account for the changing of the orderings of the edges
                    vertices_with_swapped_edges.update(
                        vertices_to_new_vertex.intersection(G2.neighbors_in(target_vertex)))

                    power += len(vertices_with_swapped_edges)

                    if power % 2 != 0:
                        coefficient = -coefficient

                    graphs.append((coefficient, G1, G2.canonical_label()))
                    print('I = {}, J = {}, l = {} has coefficient {}'.format(I, J, l, coefficient))

        from .kontsevichgraph_generators import kontsevichgraphs
        print('Generating equation')
        expr = 0
        for i, term in enumerate(graphs):
            if len(term) == 2:
                coeff, graph1 = term
                graph2 = None
            else:
                coeff, graph1, graph2 = term

            graph_var = 1
            for G in graph1, graph2:
                if G is None:
                    continue
                try:
                    weight = kontsevichgraphs.lookup_weight(G, do_calc=False)
                except (AttributeError, NotImplementedError, TypeError):
                    weight = None
                if weight is None:
                    var_name = '{}_{}'.format(G.internal_order(), G.external_order())
                    for E in G.edges():
                        var_name += '_' + str(E[0]) + str(E[1])
                    import re
                    graph_plot = G.plot(figsize=max(2, G.internal_order()+0.5))
                    graph_str = latex(graph_plot)
                    graph_str = re.sub('%[^\r\n]*$', '', graph_str, flags=re.MULTILINE)
                    graph_str = graph_str.replace('\n', ' ')
                    graph_str = r'$\left(\raisebox{{-0.5\height}}{{{}}}\right)$'.format(graph_str)
                    graph_var *= SR.var('w_'+var_name, latex_name=graph_str)
                else:
                    coeff *= weight
            if return_latex:
                # Hack to ensure that when using view, numbers are displayed correctly
                # We have to ensure that they are in a math environment, as if
                # one of the graphs is drawn using pgfpicture, this is dropped.
                # We add the pgfpicture environment to ensure this is the case
                coeff_var = SR.var('c_{}'.format(i), latex_name=r'\Huge ${}$ \begin{{pgfpicture}}\end{{pgfpicture}}'.format(latex(coeff)))
                expr += coeff_var*graph_var
            else:
                expr += coeff * graph_var
        return expr, graphs
