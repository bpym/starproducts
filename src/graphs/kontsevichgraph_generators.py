r"""Generator class for Kontsevich graphs.

We provide lists of all graphs of a given order, loading them from a cache
file along with their weights. We provide methods to generate specific
classes of Kontsevich graphs (e.g. Bernoulli graphs, wedges graphs etc.)

NOTES:

When this file is loaded, we try and load the graph cache from
``$SAGE_SHARE/graphs/kontsevich.sobj``. If we cannot find this file, then no
pre-computed weights will be available. We also generate a warning on loading
this file if this is the case.

EXAMPLES:

List all order 3 graphs
::

    sage: G3 = kontsevichgraphs.list(3)

Generate the Felder-Willwacher graph
::

    sage: G = kontsevichgraphs.FelderWillwacher()
    sage: G.edges()
    [('int0', 'ext0', None),
     ('int0', 'int1', None),
     ('int1', 'ext0', None),
     ('int1', 'int2', None),
     ('int2', 'ext0', None),
     ('int2', 'int3', None),
     ('int3', 'ext0', None),
     ('int3', 'int4', None),
     ('int4', 'ext1', None),
     ('int4', 'int5', None),
     ('int5', 'ext1', None),
     ('int5', 'int6', None),
     ('int6', 'ext0', None),
     ('int6', 'ext1', None)]

"""
import os

from collections import defaultdict
from subprocess import Popen, PIPE
from warnings import warn

from sage.env import SAGE_SHARE
from sage.functions.other import ceil
from sage.rings.integer_ring import ZZ
from sage.structure.sage_object import SageObject
from sage.misc.persist import load, save

from sage.misc.lazy_list import lazy_list_generic

from .kontsevichgraph import (KontsevichAdmissibleGraph, KontsevichFormalityGraph,
                             KontsevichRelationGraph, KontsevichQuantizationGraph)

_CACHE_FILES = {
    'default': 'kontsevichgraphs_harmonic.sobj',
    'harmonic': 'kontsevichgraphs_harmonic.sobj',
    'logarithmic': 'kontsevichgraphs_logarithmic.sobj',
    'tfamily': 'kontsevichgraphs_tfamily.sobj'
}

_CACHE_FILES = {K: os.path.join(SAGE_SHARE, 'graphs', _CACHE_FILES[K]) for K in _CACHE_FILES}

def get_iso_classes(graph_list):
    r"""Return a list with one element of each isomorphism class that
    appears in ``graph_list``.

    INPUT:

    - ``graph_list`` -- a list of graphs

    OUTPUT:

    A (minimal) list of graphs such that if $G$ is in the list, then $G$ is
    isomorphic to an element in the new list. Each element of this list
    will be immutable.

    ALGORITHM:

    We calculate a canonical form for each graph, then store an entry in a
    dictionary for every graph we have. We then return a list of keys of
    that dictionary.

    EXAMPLES:

    A typical example
    ::

        sage: from graphs.kontsevichgraph_generators import get_iso_classes
        sage: L = [ graphs.BullGraph(), graphs.BubbleSortGraph(2), Graph([ [1, 2, None ] ])]
        sage: M = get_iso_classes(L)
        sage: len(M)
        2
        sage: for G in L:
        ....:     for k, H in enumerate(M):
        ....:         if G.is_isomorphic(H):
        ....:             print("Found {} at {}".format(G, k))
        Found Bull graph at 0
        Found Bubble sort at 1
        Found Graph on 2 vertices at 1

    An empty input gives an empty output
    ::

        sage: from graphs.kontsevichgraph_generators import get_iso_classes
        sage: get_iso_classes([])
        dict_keys([])
    """
    classes = {}
    for graph in graph_list:
        key = graph.canonical_label().copy(immutable=True)
        classes[key] = True

    return classes.keys()

class LazyMapList(lazy_list_generic):
    """Generate a list on the fly using pre-loaded data.

    This class allows delaying expensive construction operations until the user
    requests the data in that form. When an instance is created, a construction
    function is specified along with a list of data. When items are accessed,
    the function is called with each successive item of data in turn.
    """

    def __init__(self, function, data, slice_len=100):
        """Return a Lazy Map List.

        INPUT:

        - ``function`` -- a function to be called to transform the input data
          into output data
        - ``data`` -- the input data
        - ``slice_len`` -- integer (default: `100`); how many items to process
          at a time.

        OUTPUT: a lazy list.

        EXAMPLES:

        Given a list of numbers, find the first prime ::

            sage: from graphs.kontsevichgraph_generators import LazyMapList
            sage: input_data = range(105, 30000)
            sage: L = LazyMapList(is_prime, input_data)
            sage: for n, b in enumerate(L):
            ....:     if b:
            ....:         break
            sage: if b:
            ....:     print("prime found at index {}".format(n))
            prime found at index 2
        """
        self.function = function
        self.data = data
        self.slice_len = slice_len

        self.i = 0
        lazy_list_generic.__init__(self)

    def _new_slice(self):
        new_slice = list(map(self.function, self.data[self.i:self.i+self.slice_len]))
        self.i += self.slice_len
        return new_slice

    def __len__(self):
        return len(self.data)

class KontsevichGraphGenerators(SageObject):
    """List Kontsevich graphs of various types.

    The main function in this class is ``list``. This generates a list of
    Kontsevich graph according to various options passed to it (order, whether
    to just return representatives of isomorphism classes etc).

    We store a cache of all the graphs we have generated which can be saved or
    loaded using the save_cache/load_cache function. By default we load the
    cache from a file called kontsevich_graphs.sobj in the current directory.

    .. SEEALSO::

        :meth:`list`
    """

    # We include two methods here simply to save repetitive typing.
    # all order-4 graphs with non-zero weight:
    # kontsevichgraphs.list(4, kontsevichgraphs.filter_weightless)
    # instead of
    # kontsevichgraphs.list(4, lambda G: G._cached_weight != 0)
    # or
    # [ G for G in kontsevichgraphs.list(4) if G._cached_weight != 0 ]
    # (but tab completion makes it worth it :-) )

    @staticmethod
    def filter_weightless(G):
        """Check if a ``KontsevichQuantizationGraph`` is known to have weight 0.

        This is designed to be used as the ``filter`` argument to ``list``.

        INPUT:

        - ``G`` -- A ``KontsevichQuantizationGraph``

        OUTPUT:

        The value ``True`` if ``G`` is already known not to have 0 weight, and
        ``False`` otherwise. This relies on the weight having already been
        calculated: if it has not, then ``False`` is returned.

        EXAMPLES:

        Check a Bernoulli graph with an even number of vertices has 0 weight.
        ::

            sage: from graphs.kontsevichgraph_generators import KontsevichGraphGenerators
            sage: G = kontsevichgraphs.Bernoulli(3)
            sage: KontsevichGraphGenerators.filter_weightless(G)
            True
            sage: G.weight()
            0
            sage: KontsevichGraphGenerators.filter_weightless(G)
            False

        .. SEEALSO::

                :meth:`list`
        """
        return bool(G._cached_weight != 0)

    def __init__(self, filename=None):
        """Create this object and set up the default cache filename.

        INPUT:

        - ``filename`` (optional) - the file to load the cache from. If not
          given, use the value ``_DEFAULT_CACHE_FILENAME``

        OUTPUT:

        An object *without* the cache loaded.

        EXAMPLES::

            sage: from graphs.kontsevichgraph_generators import KontsevichGraphGenerators
            sage: kontsevichgraphs = KontsevichGraphGenerators()
        """

        # Cache of all Kontsevich graphs of various properties
        self._cache = {}
        # File containing cache, stored using Sage's save() function
        if filename is None:
            self._cache_filename = _CACHE_FILES['default']
        else:
            self._cache_filename = filename

    def Wedge(self, n=1):
        """Create a wedge product graph.

        INPUT:

        - ``n`` -- (default 1) the number of wedges to combine

        OUTPUT:

        A Kontsevich graph with ``n`` interior vertices, each connected to both
        exterior vertices.

        EXAMPLES::

            sage: G = kontsevichgraphs.Wedge(2)
            sage: G.edges()
            [('int0', 'ext0', None),
             ('int0', 'ext1', None),
             ('int1', 'ext0', None),
             ('int1', 'ext1', None)]

        It is an error to use a non-integer or negative value for ``n``
        ::

            sage: kontsevichgraphs.Wedge(-1)
            Traceback (most recent call last):
            ...
            ValueError: n must be a non-negative integer (received: -1)

        .. PLOT::

           G = kontsevichgraphs.Wedge(3)
           sphinx_plot(G)
        """
        if not n in ZZ or n < 0:
            raise ValueError(
                "n must be a non-negative integer (received: {})".format(n))
        return KontsevichQuantizationGraph([['ext0', 'ext1']] * n)

    def Chain(self, n=1):
        """Create a graph consisting of n vertices connected in a chain of
        2-cycles.

        INPUT:

        - ``n`` -- (default 1) the number of vertices to be connected together
          in this chain

        OUTPUT:

        A Kontsevich graph with ``n`` internal vertices. The vertices 2 and n+1
        are connected to L and R respectively. Then, each vertex is connected
        to the adjacent vertex on each side of it i.e. vertex i is connected to
        vertices i-1 and i+1. This creates a series of 2-cycles along the
        length of the graph.

        EXAMPLES::

            sage: G = kontsevichgraphs.Chain(-1)
            Traceback (most recent call last):
            ...
            ValueError: n must be a non-negative integer (received: -1)

        .. PLOT::

                G = kontsevichgraphs.Chain(3)
                sphinx_plot(G)
        """
        if n < 1:
            raise ValueError(
                "n must be a non-negative integer (received: {})".format(n))
        if n == 1:
            return self.Wedge(1)
        if n == 2:
            return KontsevichQuantizationGraph([['ext0', 'int1'], ['int0', 'ext1']])

        internal_edges = [['int'+ str(i - 1), 'int' + str(i + 1)] for i in range(1, n-1)]
        return KontsevichQuantizationGraph([['ext0', 'int1']] + internal_edges + [['int'+str(n-2), 'ext1']])

    def Bernoulli(self, n):
        """Generate a graph which is known to have weight equal to the nth
        Bernoulli number divided by (n!)^2

        INPUT:

        - ``n`` -- the index of the desired Bernoulli number for the weight

        OUTPUT:

        A graph which has the ``n`` th Bernoulli number as its weight.

        EXAMPLES::

            sage: G = kontsevichgraphs.Bernoulli(3)
            sage: G.weight()
            0

        .. PLOT::

            G = kontsevichgraphs.Bernoulli(4)
            sphinx_plot(G)

        """

        if n == 0:
            return self.Wedge(0)
        return KontsevichQuantizationGraph([['ext0', 'int'+str(i+1)] for i in range(n-1)] + [['ext0', 'ext1']])

    def BubbleLadder(self, n):
        """Generate a bubble ladder graph.

        This family of graphs is conjectured to have the largest weight amongst
        all graphs whose differential operator does not vanish for a generic
        quadratic Poisson bracket when applied to a coordinate function.

        INPUT:

        - ``n`` -- the number of ``rungs'' in the ladder.

        OUTPUT: A bubble ladder graph.

        .. PLOT::

            G = kontsevichgraphs.BubbleLadder(3)
            sphinx_plot(G)
        """
        if n <= 1:
            return self.Wedge(n)

        def to_internal(*L):
            """Convert a list of integers into names of the corresponding internal vertex.

            This simply prepends the string `int` to each number."""
            return ['int' + str(n) for n in L]

        out_neighbours = [['int1', 'int2']]
        out_neighbours += sum([[to_internal(2*k+2, 2*k+3), to_internal(2*k+1, 2*k+4)] for k in range(n-1)], [])
        out_neighbours[-2][1] = 'ext0'
        out_neighbours[-1][1] = 'ext1'
        return KontsevichQuantizationGraph(out_neighbours, format='out_neighbours')

    def FelderWillwacher(self):
        r"""Generate the Felder-Willwacher graph which has weight (up to
        rational factors) $\Zeta_3^2 / pi^6$.

        INPUT:

        None

        OUTPUT:

        The graph described in "On the (Ir)rationality of Kontsevich Weights"
        (Felder & Willwacher, 2009) in which they show the graph has weight
        equal to a multiple of $\Zeta_3$.

        EXAMPLES::

            sage: G = kontsevichgraphs.FelderWillwacher(); G
            Kontsevich graph (Quantization) on (7, 2) vertices
            sage: G.edges()
            [('int0', 'ext0', None),
             ('int0', 'int1', None),
             ('int1', 'ext0', None),
             ('int1', 'int2', None),
             ...
             ('int6', 'ext1', None)]
        """
        G = KontsevichQuantizationGraph(
            [['ext0', 'int1'], ['ext0', 'int2'], ['ext0', 'int3'], ['ext0', 'int4'],
             ['ext1', 'int5'], ['ext1', 'int6'], ['ext0', 'ext1']])
        from sage.rings.rational import Rational
        from sage.symbolic.constants import pi
        from sage.functions.transcendental import zeta
        G._cached_weight = Rational((1, 2903040))/pi**6 * (13*pi**6 - 11340*zeta(3)**2)
        return G

    def Random(self, n):
        """Return a random graph of a specified internal order.

        INPUT:

        - ``n`` -- the desired internal order

        OUTPUT:

        A random ``KontsevichQuantizationGraph`` with the specified internal
        order.
        """
        L = self.list(n)
        from sage.misc.prandom import randrange
        return L[randrange(len(L))]

    def list(self, internal_order=None, external_order=2, edges=None,
             graph_type=KontsevichQuantizationGraph, graph_filter=None):
        """Generate a list of Kontsevich graphs of a given order.

        INPUT:

        - ``order`` -- the order of the graphs to generate
        - ``graph_filter`` (optional) -- if given, return only graphs ``G`` for
          which ``graph_filter(G)`` is ``True``.

        OUTPUT:

        A list of all Kontsevich graphs of the given order, filtered by the
        function if given. If the graphs exists in the cache file, then these
        are returned.

        EXAMPLES:

        Generate a list of order-2 graphs::

            sage: list(kontsevichgraphs.list(2))
            [Kontsevich graph (Quantization) on (2, 2) vertices,
             Kontsevich graph (Quantization) on (2, 2) vertices,
             Kontsevich graph (Quantization) on (2, 2) vertices]

        It is an error for ``order`` to not be a non-negative integer::

            sage: kontsevichgraphs.list(0)
            [Kontsevich graph (Quantization) on (0, 2) vertices]
            sage: kontsevichgraphs.list(pi)
            Traceback (most recent call last):
            ...
            ValueError: n must be a non-negative integer (received: pi)
            sage: kontsevichgraphs.list(-1)
            Traceback (most recent call last):
            ...
            ValueError: n must be a non-negative integer (received: -1)
        """

        if internal_order is None:
            if edges is None:
                raise RuntimeError("Must specify one of internal_order and edges!")
            if issubclass(graph_type, KontsevichQuantizationGraph):
                if edges % 2 != 0:
                    raise RuntimeError("Quantization graphs must have an even number of edges")
                orders = [(edges/2, 2)]
            elif issubclass(graph_type, KontsevichFormalityGraph):
                orders = [(n, edges+2-2*n) for n in range((edges+2)/2+1)]
            elif issubclass(graph_type, KontsevichRelationGraph):
                orders = [(n, edges+3-2*n) for n in range((edges+3)/2+1)]
            else:
                raise RuntimeError("Can only list graphs by edge count for formality or relation graphs")
            return sum([self.list(n, m, None, graph_type, graph_filter) for n, m in orders], [])

        key = (internal_order, external_order)
        if internal_order == 0:
            try:
                L = [graph_type(external_order=external_order, immutable=True)]
            except RuntimeError:
                L = []
            return L

        use_cache = issubclass(graph_type, KontsevichFormalityGraph)
        graph_list = None
        if use_cache:
            try:
                graph_list = self._cache[key]
            except KeyError:
                pass

        if graph_list is None:
            graph_list = KontsevichGraphGenerators._gen_graphs(internal_order, external_order, graph_type)
            if use_cache:
                self._cache[key] = graph_list

        if graph_filter is None:
            return graph_list
        else:
            return filter(graph_filter, graph_list)

    @staticmethod
    def _cached_form(G, certificate=False):
        # This function is currently broken as canonical labelling does not
        # agree with the results returned from the dig6 string
        H, iso = G.canonical_label(certificate=True)
        dig6_str = H.dig6_string()
        try:
            H = KontsevichQuantizationGraph(dig6_str, format='dig6', immutable=True)
        except RuntimeError:
            H = KontsevichAdmissibleGraph(dig6_str, format='dig6', immutable=True)
        if certificate:
            return H, iso
        return H

    def lookup_weight(self, G, do_calc=True):
        """Try to find the weight of a graph in our cache.

        INPUT:

        - ``G`` -- the graph to look up.
        - ``do_calc`` -- if we don't find the graph, calculate the weight using
          ``KontsevichQuantizationGraph.weight``

        OUTPUT:

        If the graph is found in the cache, the weight of that graph. If not, and
        ``do_calc`` is ``True``, then the result of ``G.weight()``. If not, and
        ``do_calc`` is ``False``, then ``None``.

        EXAMPLES:

        Check the weight of the Bernoulli graph of order 2.
        ::

            sage: G = kontsevichgraphs.Bernoulli(2)
            sage: kontsevichgraphs.lookup_weight(G)
            1/12

        TESTS:

        Try generating a graph, setting its weight in the cache and looking it up.
        ::

            sage: Q3 = kontsevichgraphs.list(3)
            sage: # The graph to check
            sage: H = KontsevichQuantizationGraph([['ext0', 'ext1'], ['int0', 'int2'], ['int1', 'ext1']])
            sage: # Find the representative of the graph
            sage: for G in Q3:
            ....:     if G.is_isomorphic(H): break
            sage: # Get the isomorphism so we can calculate the sign change
            sage: is_iso, iso = G.is_isomorphic(H, certificate=True)
            sage: _test_value = 10
            sage: G._cached_weight = _test_value
            sage: G.isomorphism_weight_effect(iso)(kontsevichgraphs.lookup_weight(H)) == _test_value
            True
        """
        if G._cached_weight != None:
            return G._cached_weight

        if G.weight_vanishes():
            return 0

        H, iso = KontsevichGraphGenerators._cached_form(G, True)
        weight = None
        key = (H.internal_order(), H.external_order())
        try:
            index = list(self._cache[key]).index(H)
            if self._cache[key][index]._cached_weight is not None:
                if not isinstance(G, KontsevichQuantizationGraph):
                    G = KontsevichQuantizationGraph(G)
                weight = G.isomorphism_weight_effect(iso)(
                    self._cache[key][index]._cached_weight)
                G._cached_weight = weight
        except (KeyError, ValueError, RuntimeError):
            pass

        if weight is None and do_calc:
            weight = G.weight()

        return weight

    @staticmethod
    def _setup_partition(G, n, m, graph_type):
        """Given a graph, find the external vertices and set a partition on G."""
        out_degrees = G.out_degree()
        vertices = G.vertices()

        if out_degrees.count(0) < m:
            return []
        possible_ext_vertices = set()
        last_index = -1
        # @TODO this may miss graphs with internal vertices with no outgoing edges
        try:
            while True:
                last_index = out_degrees.index(0, last_index+1)
                possible_ext_vertices.add(vertices[last_index])
        except ValueError:
            pass

        def _do_setup_partition(G, ext_vertices):
            int_vertices = set(vertices).difference(ext_vertices)
            partition = defaultdict(set, {'ext': ext_vertices, 'int': int_vertices})

            H = graph_type(G, partition=partition)

            try:
                H.validate()
            except RuntimeError:
                return None

            return H.canonical_label().copy(immutable=True)

        from sage.combinat.combination import Combinations
        return [_do_setup_partition(G, ext_vertices) for ext_vertices in Combinations(possible_ext_vertices, m)]

    @staticmethod
    def _gen_graphs(n, m, graph_type=KontsevichAdmissibleGraph):
        """Generate a list of all Kontsevich graphs of order (n, m) using calls
        to nauty programs.

        INPUT:

        - ``n`` -- the internal order of graphs to generate
        - ``m`` -- the external order of graphs to generato

        OUTPUT:

        A list of all Kontsevich admissible graphs with ``n`` internal vertices
        and ``m`` external vertices.

        ALGORITHM:

        We first (using ``geng``) generate all connected undirected graphs of
        order $n+2$, with between $n$ and $2n$ edges. Next, we use ``directg``
        to replace each edge with all possible combinations of directed edges,
        then filter so the total number of edges is $2n$. Finally, we use
        ``pickg`` to filter out to only leave graphs with $n$ vertices of
        out-degree $2$ and $2$ vertices of out-degree $0$.
        """


        def _run_pipes(*args):
            if not args:
                return None

            p = dict()
            p[0] = Popen(args[0], stdout=PIPE, stderr=PIPE)
            for i in range(1, len(args)):
                p[i] = Popen(args[i], stdin=p[i-1].stdout, stdout=PIPE, stderr=PIPE)
                p[i-1].stdout.close()
                p[i-1].stderr.close()
            return p[len(args)-1].communicate()[0]

        if not n in ZZ or n < 0:
            raise ValueError("n must be a non-negative integer (received: {})".format(n))

        if not 2*n+m > 0:
            raise RuntimeError("Require 2*n+m>0")

        if not issubclass(graph_type, KontsevichAdmissibleGraph):
            raise RuntimeError("I only know how to generate admissible Kontsevich graphs")

        if graph_type == KontsevichAdmissibleGraph:
            output = _run_pipes(('geng', str(n+m)), 'directg')
        else:
            if issubclass(graph_type, KontsevichFormalityGraph):
                edge_count = 2*n+m-2
            elif issubclass(graph_type, KontsevichRelationGraph):
                edge_count = 2*n+m-3
            else:
                raise RuntimeError("Unknown graph type")
            edge_min = ceil(edge_count/2)
            edge_max = edge_count

            if issubclass(graph_type, KontsevichQuantizationGraph):
                if m != 2:
                    raise RuntimeError("Can only generated quantization graphs if m==2")
                pickg = [('pickg', '-d0', '-m2', '-D2', '-M{}'.format(n))]
            else:
                pickg = []

            output = _run_pipes(('geng', str(n + m), "{}:{}".format(edge_min, edge_max)),
                                ('directg', '-e{}'.format(edge_count)),
                                *pickg)

        # Strip off leading ampersands as Sage's digraph6 handler doesn't
        # handle these
        from sage.graphs.digraph import DiGraph
        graphs = [DiGraph(L[1:], format='dig6') for L in output.splitlines()]

        # Convert to Kontsevich graphs
        output_len = len(graphs)
        graphs = sum([KontsevichGraphGenerators._setup_partition(G, n, m, graph_type) for G in graphs], [])
        take_iso_classes = len(graphs) > output_len
        graphs = [G for G in graphs if G is not None]
        if take_iso_classes:
            graphs = get_iso_classes(graphs)
        return graphs

    # @TODO make class decorator?
    def load_cache(self, filename=None, no_defaults=False):
        """Load a list of cached graph from a file.

        If no filename is given, we use the last filename used for either
        saving or loading (defaulting to "kontsevichgraphs").

        INPUT:

        - ``filename`` (optional) -- the file to load the cache from.

        OUTPUT:

        None

        .. SEEALSO::

            :meth:`save_cache`

        EXAMPLES::
            sage: x, y = var('x, y')
            sage: from sage.misc.temporary_file import tmp_filename
            sage: fname = tmp_filename(ext='.sobj')
            sage: kontsevichgraphs.clear_cache()
            sage: L = kontsevichgraphs.list(1)
            sage: L[0]._cached_weight = x
            sage: kontsevichgraphs.save_cache(fname)
            sage: L[0]._cached_weight = y
            sage: kontsevichgraphs.list(1)[0]._cached_weight
            y
            sage: kontsevichgraphs.load_cache(fname)
            sage: kontsevichgraphs.list(1)[0]._cached_weight
            x
        """
        if not no_defaults:
            if filename is None:
                filename = self._cache_filename
            elif filename in _CACHE_FILES:
                filename = _CACHE_FILES[filename]

        def cachevalue_to_graph(L):
            """Convert a tuple to a graph with specified cached weight."""
            return KontsevichQuantizationGraph(data=L[0], format='dig6', immutable=True,
                                               weight=L[1], check=False)

        is_text = False
        lines = []
        try:
            with open(filename, 'rb') as f:
                L0 = f.readline()
                if L0.startswith(b'&&'):
                    # Read in a text file
                    lines = [L0] + f.readlines()
                    f.close()
                    is_text = True
        except IOError:
            pass

        if is_text:
            K = None
            cache = {}
            graphs = []
            from sage.symbolic.ring import SR
            for line in lines:
                if line.startswith(b'&&'):
                    if K is not None:
                        cache[K] = graphs
                        graphs = []
                    K = tuple(int(x) for x in line[2:].split(' ', 1))
                else:
                    dig6_string, weight_string = line.split(' ', 1)
                    weight = SR(weight_string)
                    graphs.append(cachevalue_to_graph([dig6_string, weight]))
            self._cache = cache
        else:
            cache_dig6 = load(filename)
            self._cache_filename = filename
            self._cache = {(K, 2): LazyMapList(cachevalue_to_graph, cache_dig6[K]) for K in cache_dig6}

    def save_cache(self, filename=None, format='binary'):
        """Save a list of cached graph from a file.

        If no filename is given, we use the last filename used for either
        saving or loading in binary format (defaulting to "kontsevichgraphs").

        The file can be saved as either a binary file, using ``save`` and
        ``load``,or as a text file. The format can be specified using the
        ``format`` argument.

        INPUT:

        - ``filename`` (optional) -- the file to save the cache to.
        - ``format`` (optional) -- the format to use for the file.

        OUTPUT:

        None

        .. SEEALSO::

            :meth:`load_cache`

        EXAMPLES::
            sage: x, y = var('x, y')
            sage: from sage.misc.temporary_file import tmp_filename
            sage: fname = tmp_filename(ext='.sobj')
            sage: kontsevichgraphs.clear_cache()
            sage: L = kontsevichgraphs.list(1)
            sage: L[0]._cached_weight = x
            sage: kontsevichgraphs.save_cache(fname)
            sage: L[0]._cached_weight = y
            sage: kontsevichgraphs.list(1)[0]._cached_weight
            y
            sage: kontsevichgraphs.load_cache(fname)
            sage: kontsevichgraphs.list(1)[0]._cached_weight
            x
        """
        if not format in ['binary', 'text']:
            raise ValueError("Format must be one of 'binary', 'text' (recieved: {})".format(format))

        if filename is None:
            filename = self._cache_filename
        else:
            if format == 'binary':
                self._cache_filename = filename

        if format == 'binary':
            cache_dig6 = {K[0]: [(G.dig6_string(), G._cached_weight) for G in self._cache[K]] for K in self._cache}
            save(cache_dig6, filename)
        else:
            lines = sum([['&&{} {}\n'.format(K[0], K[1])] +
                        ["{} {}\n".format(G.dig6_string(), G._cached_weight) for G in self._cache[K]]
                        for K in self._cache], [])

            with open(filename, 'w') as output_file:
                output_file.writelines(lines)

    def merge_maple_cache(self, maple_file):
        """Merges a list of maple graphs into the cache.

        Should only be used for converting .mpl files to cache files.
        Expects a text file with lines formatted like

        w[[[L,R],[L,R],[4,5],[1,R],[2,L]]]:=23/23040;

        Note that since cache keys are by weight, the entire list of graphs for
        a given weight will be replaced.

        INPUT: 
        - ``maple_file`` -- the .mpl file with the cached graph weights.

        OUTPUT:

        None
        """
        import re

        graph_matcher = re.compile(r"^.+?\[(.*)\]")
        expression_matcher = re.compile(r"^.+?=(.*);")
        new_cache_values = defaultdict(list)
        with open(maple_file, 'r') as in_file:
            for line in in_file.readlines():
                graph_string = graph_matcher.findall(line)
                expression_string = expression_matcher.findall(line)
                assert(len(graph_string) == 1)
                assert(len(expression_string) == 1)

                graph, sign = KontsevichAdmissibleGraph._from_maple(
                        graph_string[0], calc_sign=True)
                weight = sign * KontsevichAdmissibleGraph._parse_maple(expression_string[0])
                q_graph = KontsevichQuantizationGraph(data=graph, immutable=True,
                                                      weight=weight, check=True)
                new_cache_values[
                        (q_graph.internal_order(), q_graph.external_order())].append(q_graph)
        self._cache = {**self._cache, **new_cache_values}

    def clear_cache(self, condition=None):
        """Remove entries from cache that meet the given condition.

        INPUT:

        - ``condition`` (optional) -- if ``condition(n)`` is true, we
          delete the cache entry containing all graphs of order ``n``.
          n is a type of (internal vertex order, external vertex order) 

        OUTPUT:

        None

        EXAMPLES:

        Remove all graphs of order greater than 3::

            sage: kontsevichgraphs.clear_cache(lambda K: K[0] > 3)
        """
        if condition is None:
            self._cache = {}
        else:
            keys = self._cache.keys()
            for K in keys:
                if condition(K):
                    del self._cache[K]

kontsevichgraphs = KontsevichGraphGenerators()

try:
    kontsevichgraphs.load_cache()
except IOError:
    warn("Unable to load Kontsevich graph cache file '{}': weights will have to be computed".format(
        kontsevichgraphs._cache_filename))
    kontsevichgraphs._cache = {}
