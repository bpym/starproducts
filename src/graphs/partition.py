# -*- coding: utf-8 -*-
r"""
Partition graphs (graphs with vertices grouped in a partition).

This module implements the base partition graph (a subclass of GenericGraph) and
also two subclasses inheriting from both this and from Graph or DiGraph.
"""
from copy import deepcopy
from collections import defaultdict

from sage.graphs.generic_graph import GenericGraph
from sage.graphs.graph import Graph
from sage.graphs.digraph import DiGraph
from sage.combinat.combination import Combinations

class PartitionGenericGraph(GenericGraph):
    """A graph, with its vertices split into arbitrary partitions.

    The partitions are stored as a dictionary, so any hashable object may be
    used as a specifier. Although it is technically possible to use ``None`` as
    a key, this is highly discourgaed. The function :meth:`find_vertex` returns
    ``None`` if it cannot find the given vertex, and so this may lead to
    confusion.
    """
    def __init__(self, data=None, partition=None, check=True):
        r"""Construct a new partitioned graph.

        INPUT:

        - ``data`` -- the information used to construct the graph.
        - ``partition`` -- a dictionary specifying the initial partition of the
          graph
        - ``check`` -- If ``True``, check that the arguments are valid, and
          raise an error if they are not.

        This constructor only handles setting up the partition. It tries to do
        so using the following:

        1. The argument ``partition``. If specified, this is treated as a
           dictionary, with each key being the label for the partition and
           each entry a set of vertices in that partition.
        2. If ``data`` is a ``PartitionGenericGraph``, we use the partition
           from there.
        3. If ``data`` is a ``GenericGraph``, we assign all vertices to the
           partition labelled 0.

        If ``check`` is ``True``, we ensure that parts of the partition are
        pairwise disjoint.

        EXAMPLES:

        Create a graph by specifying the partition::

            sage: G = PartitionGraph(Graph(5), partition={0: {0, 1, 4}, 1: {2, 3}})
            sage: G.partition
            defaultdict(<class 'set'>, {0: {0, 1, 4}, 1: {2, 3}})
            sage: H = PartitionGraph(G); H.partition
            defaultdict(<class 'set'>, {0: {0, 1, 4}, 1: {2, 3}})

        The partition may be specified as a dictionary of any iterable::

            sage: G = PartitionGraph(Graph(5), partition={0: range(3), 1: (3, 4)})
            sage: G.partition
            defaultdict(<class 'set'>, {0: {0, 1, 2}, 1: {3, 4}})

        A partition must consist of disjoint parts::

            sage: G = PartitionGraph(Graph(3), partition={0: [0], 1: [0,1]})
            Traceback (most recent call last):
            ...
            RuntimeError: Invalid partition

        If no data is provided, we need an empty partition::

            sage: G = PartitionGraph(partition={0: [1]})
            Traceback (most recent call last):
            ...
            RuntimeError: Invalid partition

        This check can be disabed::

            sage: G = PartitionGraph(Graph(9), partition={0: [0], 1: range(10)}, check=False)
            sage: G.partition
            defaultdict(<class 'set'>, {0: {0}, 1: {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}})
        """
        if data is None:
            if check and partition is not None and partition.keys():
                raise RuntimeError("Invalid partition")

        if partition is not None:
            self.partition = defaultdict(set, {K: set(partition[K]) for K in partition})
        elif isinstance(data, PartitionGenericGraph):
            self.partition = deepcopy(data.partition)
        elif isinstance(data, GenericGraph) and partition is None:
            self.partition = defaultdict(set, {0: set(data.vertices())})
        else:
            self.partition = defaultdict(set)

        if check:
            combos = Combinations(self.partition.keys(), 2)
            if any([self.partition[K] & self.partition[L] for K, L in combos]):
                raise RuntimeError("Invalid partition")

    # __add__ handled by disjoint_union

    def __eq__(self, other):
        """
        Compare self and other for equality.

        Do not call this method directly. That is, for ``G.__eq__(H)``
        write ``G == H``.

        Two partitioned graphs are considered equal if the following hold:
         - they are either both directed, or both undirected;
         - they have the same settings for loops, multiedges, and
           weightedness;
         - they have the same set of vertices;
         - they have the same (multi)set of arrows/edges, where labels
           of arrows/edges are taken into account if *and only if*
           the graphs are considered weighted. See
           :meth:`~GenericGraph.weighted`;
         - for every vertex, the partition in which it lies is the same for
           both graphs.

        Note that this is *not* an isomorphism test.

        EXAMPLES::

            sage: G = graphs.PetersenGraph()
            sage: partition = {'inner': range(5, 10), 'outer': range(5)}
            sage: H = PartitionGraph(G, partition=partition)
            sage: G == H
            False
            sage: partition['empty'] = []
            sage: H2 = PartitionGraph(G, partition=partition)
            sage: H == H2
            True
            sage: H2 == H
            True
            sage: H2 = PartitionGraph(G, partition={0: range(10)})
            sage: H == H2
            False
            sage: H2 == H
            False
        """
        if not isinstance(other, PartitionGenericGraph):
            return False

        # Check that any non-empty partitions correspond
        if any([self.partition[K] != other.partition[K] for K in self.partition if self.partition[K]]):
            return False

        # This point may be reached while other.partition has a key that is not
        # in self.partition. However, if this is non-empty, then the check for
        # equality of vertices will fail.
        return super(PartitionGenericGraph, self).__eq__(other)

    def __hash__(self):
        """
        Computes a hash for self, if self is immutable.

        Only immutable graphs are hashable. The resulting value is cached.

        EXAMPLES::

            sage: G = graphs.PetersenGraph()
            sage: H = PartitionGraph(G, partition={'inner': range(5, 10), 'outer': range(5)})
            sage: {H:1}[H]
            Traceback (most recent call last):
            ...
            TypeError: This graph is mutable, and thus not hashable. Create
            an immutable copy by `g.copy(immutable=True)`
            sage: G_imm = G.copy(immutable=True)
            sage: G_imm == G
            True
            sage: {G_imm:1}[G_imm]  # indirect doctest
            1
            sage: G_imm.__hash__() is G_imm.__hash__()
            True
        """
        hash_value = super(PartitionGenericGraph, self).__hash__()
        for pair in self.partition.items():
            hash_value ^= hash((pair[0], frozenset(pair[1])))
        return hash_value

    def _repr_(self):
        """
        Return a string representation of self.

        EXAMPLES::

            sage: G = graphs.PetersenGraph()
            sage: H = PartitionGraph(G, partition={'inner': range(5, 10), 'outer': range(5)})
            sage: H._repr_()
            "Partitioned Petersen graph: Graph on 10 vertices with partitions dict_keys(['inner', 'outer'])"
        """
        name = super(PartitionGenericGraph, self)._repr_()
        return 'Partitioned {} with partitions {}'.format(name, self.partition.keys())

    def partition_list(self, list_of_sets=False):
        """Return the partition as a list of lists.

        INPUT:

        - ``list_of_sets`` -- if ``True``, return a list of sets rather than
          a list of lists

        OUTPUT:

        A list, with each element a list (or set) of vertices in that part.

        EXAMPLES::

            sage: G = graphs.PetersenGraph()
            sage: H = PartitionGraph(G, partition={'inner': range(5, 10), 'outer': range(5)})
            sage: H.partition_list() # random - output may be in any order
            [[0, 1, 2¸ 3, 4], [5, 6, 7, 8, 9]]
            sage: H.partition_list(True)
            dict_values([{5, 6, 7, 8, 9}, {0, 1, 2, 3, 4}])
        """
        if list_of_sets:
            return self.partition.values()
        return [list(S) for S in self.partition.values()]

    def find_part(self, vertex):
        """Find the part of the partition which contains the specified vertex.

        INPUT:

        - ``vertex`` -- the vertex to find

        OUTPUT:

        The key of the partition containing the vertex, or None if the
        partition cannot be found.

        EXAMPLES::

            sage: G = PartitionGraph(graphs.CycleGraph(4), partition={0: [0, 1], 1: [3,4]})
            sage: G.find_part(1)
            0
            sage: G.find_part(2) is None
            True
            sage: G.find_part(3)
            1
        """
        for part in self.partition:
            if vertex in self.partition[part]:
                return part
        return None

    def add_vertex(self, part, name=None):
        """
        Create an isolated vertex. If the vertex already exists, then
        nothing is done.

        INPUT:

        - ``part`` -- key representing the partition which this vertex will be
          added to.

        - ``name`` -- (default: ``None``) name of the new vertex.  If no name
          is specified, then the vertex will be represented by the least
          non-negative integer not already representing a vertex.  Name must
          be an immutable object and cannot be ``None``.

        As it is implemented now, if a graph `G` has a large number
        of vertices with numeric labels, then ``G.add_vertex()`` could
        potentially be slow, if name is ``None``.

        OUTPUT:

        - If ``name``=``None``, the new vertex name is returned. ``None`` otherwise.

        EXAMPLES:

        ::

            sage: G = PartitionGraph()
            sage: G.add_vertex('L')
            0
            sage: G.add_vertex('R')
            1
            sage: G.vertices()
            [0, 1]
            sage: G.partition['L'] == {0}
            True
            sage: G.partition['R'] == {1}
            True

        The object ``part`` must be hashable::

            sage: G = PartitionGraph()
            sage: G.add_vertex([1, 2], 'bad_vertex')
            Traceback (most recent call last):
            ...
            TypeError: unhashable type: <class 'list'>
            sage: G.add_vertex((1, 2), 'good_vertex')
            sage: G.find_part('good_vertex')
            (1, 2)

        The vertex must not already be in a different partition::

            sage: G = PartitionGraph()
            sage: G.add_vertex('L', 1)
            sage: G.add_vertex('R', 1)
            Traceback (most recent call last):
            ...
            RuntimeError: Cannot add duplicate vertex to other partition
            sage: G.add_vertex('L', 1)
            sage: G.add_vertex('R')
            0
            sage: G.vertices()
            [0, 1]
        """
        if (name is not None) and (name in self):
            cur_part = self.find_part(name)
            assert cur_part is not None
            if cur_part == part:
                return
            else:
                raise RuntimeError(
                    "Cannot add duplicate vertex to other partition")

        try:
            hash(part)
        except:
            raise TypeError('unhashable type: {}'.format(type(part)))

        retval = GenericGraph.add_vertex(self, name)
        if retval is not None:
            name = retval

        self.partition[part].add(name)

        return retval

    def add_vertices(self, part, vertices=None):
        """
        Add vertices to the (di)graph from an iterable container of
        vertices. Vertices that already exist in the graph will not be
        added again.

        INPUT:

        - ``part``: specifies the partition to which the vertices will be added. This may be
          a single value (in which case all vertices are added to this partition), or a list of
          partitions.
        - ``vertices``: iterator of vertex labels. A new label is created, used and returned in
          the output list for all ``None`` values in ``vertices``.

        OUTPUT:

        Generated names of new vertices if there is at least one ``None`` value
        present in ``vertices``. ``None`` otherwise.

        NOTE:

        If ``part`` and ``vertices`` are specified, and one is longer than the
        other, they will both be truncated to the length of the shorter of the
        two.

        EXAMPLES::

            sage: d = {0: [1,4,5], 1: [2,6], 2: [3,7], 3: [4,8], 4: [9], 5: [7,8], 6: [8,9], 7: [9]}
            sage: p = {0: range(5), 1: range(5, 10)}
            sage: G = PartitionGraph(d, partition=p)
            sage: G.add_vertices([0,1,2], [10,11,12])
            sage: G.vertices()
            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
            sage: G.partition_list()
            [[0, 1, 2, 3, 4, 10], [5, 6, 7, 8, 9, 11], [12]]
            sage: G.add_vertices(0, [1, 10])
            sage: G.partition_list()
            [[0, 1, 2, 3, 4, 10], [5, 6, 7, 8, 9, 11], [12]]

        It is an error to specify a different partition for a pre-existing vertex
        ::

            sage: G = PartitionGraph()
            sage: G.add_vertices(0, [1,2,3])
            sage: G.add_vertices(1, [1,2,3])
            Traceback (most recent call last):
            ...
            RuntimeError: Cannot add duplicate vertex to other partition

        """
        if vertices is None:
            vertices = part
            part = 0

        # Handle partitions
        new_vertices = defaultdict(set)
        if not hasattr(part, "__iter__"):
            new_vertices[part] = set(vertices)
        else:
            for new_part, extra_vertices in zip(part, vertices):
                new_vertices[new_part].add(extra_vertices)

        # Check duplicate vertices
        # Between new_vertices and self.partition
        if any([new_vertices[K] & self.partition[L]
                for K in new_vertices for L in self.partition if not K == L]):
            raise RuntimeError("Cannot add duplicate vertex to other partition")

        for part1, part2 in Combinations(new_vertices.keys(), 2):
            if new_vertices[part1] & new_vertices[part2]:
                raise RuntimeError("Cannot add duplicate vertex to other partition")

        GenericGraph.add_vertices(self, set.union(*new_vertices.values()))
        for new_part in new_vertices:
            self.partition[new_part].update(new_vertices[new_part])

    def delete_vertex(self, vertex, in_order=False):
        """
        Deletes vertex, removing all incident edges. Deleting a non-existent
        vertex will raise an exception.

        INPUT:

        - ``vertex`` -- a vertex to delete.

        - ``in_order`` -- (default ``False``) if ``True``, this deletes the
          `i`-th vertex in the sorted list of vertices,
          i.e. ``G.vertices()[i]``.

        EXAMPLES::

            sage: B = PartitionGraph(graphs.CycleGraph(4))
            sage: B.partition
            defaultdict(<class 'set'>, {0: {0, 1, 2, 3}})
            sage: B.delete_vertex(0)
            sage: B.partition
            defaultdict(<class 'set'>, {0: {1, 2, 3}})
            sage: B.edges()
            [(1, 2, None), (2, 3, None)]
            sage: B.delete_vertex(3)
            sage: B.partition
            defaultdict(<class 'set'>, {0: {1, 2}})
            sage: B.edges()
            [(1, 2, None)]
            sage: B.delete_vertex(0)
            Traceback (most recent call last):
            ...
            ValueError: vertex (0) not in the graph

        ::

            sage: g = Graph({'a':['b'], 'c':['b']})
            sage: bg = PartitionGraph(g, partition={0: {'a', 'c'}, 1: {'b'}})
            sage: bg2 = PartitionGraph(g, partition={0: {'a', 'c'}, 1: {'b'}})
            sage: bg.vertices()
            ['a', 'b', 'c']
            sage: bg.delete_vertex('a')
            sage: bg.edges()
            [('b', 'c', None)]
            sage: bg.vertices()
            ['b', 'c']
            sage: bg2.delete_vertex(0, in_order=True)
            sage: bg2 == bg
            True
        """
        if in_order:
            vertex = self.vertices()[vertex]

        GenericGraph.delete_vertex(self, vertex)

        # An exception will have been thrown if the vertex doesn't exist
        part = self.find_part(vertex)
        if part is None:
            raise RuntimeError("Vertex ({}) not found in partitions".format(vertex))
        self.partition[part].remove(vertex)

    def delete_vertices(self, vertices):
        """
        Remove vertices from the bipartite graph taken from an iterable
        sequence of vertices. Deleting a non-existent vertex will raise an
        exception.

        INPUT:

        - ``vertices`` -- a sequence of vertices to remove.

        EXAMPLES::

            sage: B = PartitionGraph(graphs.CycleGraph(4), partition={0: range(2), 1: range(2, 4)})
            sage: B.delete_vertices([0,3])
            sage: B.partition[0]
            {1}
            sage: B.partition[1]
            {2}
            sage: B.edges()
            [(1, 2, None)]
            sage: B.delete_vertices([0])
            Traceback (most recent call last):
            ...
            ValueError: vertex (0) not in the graph
        """
        GenericGraph.delete_vertices(self, vertices)
        for vertex in vertices:
            part = self.find_part(vertex)
            if part is None:
                raise RuntimeError("Vertex ({}) not found in partitions".format(vertex))
            self.partition[part].remove(vertex)

    def plot(self, **options):
        """Return a graphics object representing the (di)graph.

        This takes the same arguments as :meth:`GenericGraph.plot`. However,
        if the keyword argument 'partition' is not specified, default to using
        the vertex partition.
        """
        if 'partition' not in options:
            options['partition'] = list(self.partition.values())
        return super(PartitionGenericGraph, self).plot(**options)

    def canonical_label(self, partition=None, certificate=None, 
                        edge_labels=False, algorithm=None, return_graph=True):
        """Return a canonical form of this graph.

        INPUT:

        As for :meth:`Graph.canonical_label`

        OUTPUT:

        A copy of this graph, labelled in a canonical way.

        If the ``partition`` argument is not supplied, we default to using the
        vertex partition. We also default to using Sage, rather than Bliss,
        for the algorithm.

        EXAMPLES::

            sage: B = graphs.ButterflyGraph()
            sage: G = PartitionGraph(B, partition={0: [0,1], 1: [2, 3, 4]})
            sage: B.canonical_label(algorithm='sage').edges()
            [(0, 3, None),
             (0, 4, None),
             (1, 2, None),
             (1, 4, None),
             (2, 4, None),
             (3, 4, None)]
            sage: G.canonical_label(algorithm='sage').edges()
            [(0, 3, None),
             (0, 4, None),
             (1, 2, None),
             (1, 4, None),
             (2, 4, None),
             (3, 4, None)]
        """
        if partition is None:
            partition = self.partition_list()

        if algorithm is None:
            algorithm = 'sage'

        return super(PartitionGenericGraph, self).canonical_label(partition, certificate, 
                                                                  edge_labels, algorithm,
                                                                  return_graph)

    # Creating a subgraph by deleting works as expected
    # If we are creating a subgraph by adding, be sure to add in partition
    def _subgraph_by_adding(self, vertices=None, edges=None, edge_property=None, immutable=None):
        H = super(PartitionGenericGraph, self)._subgraph_by_adding(vertices, edges, edge_property, immutable)
        H.partition = deepcopy(self.partition)
        vertices = set(vertices)
        H.partition = defaultdict(set, {K: self.partition[K] & vertices for K in self.partition})
        return H

    def to_simple(self, to_undirected=True, keep_label='any', immutable=None):
        if to_undirected:
            G = PartitionGraph(self)
        else:
            G = self
        return super(PartitionGenericGraph, G).to_simple(False, keep_label, immutable)

    def relabel(self, perm=None, inplace=True, return_map=False, check_input=True,
                complete_partial_function=True, immutable=None):
        res = super(PartitionGenericGraph, self).relabel(perm, inplace, True, check_input,
                                                         complete_partial_function, immutable)

        if inplace:
            G, vertex_map = self, res
        else:
            G, vertex_map = res

        G.partition = defaultdict(set, {K: {vertex_map[v] for v in self.partition[K]} for K in self.partition})

        if inplace:
            if return_map:
                res = vertex_map
            else:
                res = None
        else:
            if return_map:
                res = G, vertex_map
            else:
                res = G

        return res

class PartitionGraph(PartitionGenericGraph, Graph):
    r"""
    An undirected partition graph.

    .. SEEALSO::

        * :class:`Graph` -- for methods related to undirected graphs.
        * :class:`PartitionGenericGraph` -- for information relating to how the
          partition is stored.
    """
    def __init__(self, data=None, partition=None, check=True, *args, **kwargs):
        """
        When we create this graph, we initialise the partition first, then the
        graph structure. If the graph contains vertices that are not in the
        partition, then these are removed.

        Note that as an empty partition will be treated as a partition with a
        single part with all vertices in it.

        EXAMPLES:

        Create a graph with one partition::

            sage: G = PartitionGraph(graphs.ButterflyGraph())
            sage: G.partition
            defaultdict(<class 'set'>, {0: {0, 1, 2, 3, 4}})

        Remove extraneous vertices::

            sage: G = PartitionGraph(graphs.ButterflyGraph(), partition={0: [0, 3], 1: [1, 2]})
            sage: G.vertices()
            [0, 1, 2, 3]
        """

        PartitionGenericGraph.__init__(self, data, partition, check)

        # Replace override methods with the orignals to preserve the partition
        import types
        self.add_vertex = types.MethodType(Graph.add_vertex, self)
        self.add_vertices = types.MethodType(Graph.add_vertices, self)
        self.add_edge = types.MethodType(Graph.add_edge, self)
        self.add_edges = types.MethodType(Graph.add_edges, self)
        Graph.__init__(self, data, *args, **kwargs)
        if not self.partition.keys():
            self.partition = defaultdict(set, {0: set(self.vertices())})
        self.add_vertex = types.MethodType(self.__class__.add_vertex, self)
        self.add_vertices = types.MethodType(self.__class__.add_vertices, self)
        self.add_edge = types.MethodType(self.__class__.add_edge, self)
        self.add_edges = types.MethodType(self.__class__.add_edges, self)

        # Remove vertices not in partition (can't be done earlier as we may end
        # up in an infinite chain of recursion)
        if isinstance(data, GenericGraph):
            if self.partition.values() != []:
                vertices = set.union(*self.partition.values())
                cur_vertices = set(data.vertices())
                extra = cur_vertices.difference(vertices)
                if extra:
                    self.partition[0].update(extra)
                    self.subgraph(vertices=vertices, inplace=True)
                    self.partition[0].difference_update(extra)


class PartitionDiGraph(PartitionGenericGraph, DiGraph):
    r"""
    A directed partition graph.

    .. SEEALSO::

        * :class:`DiGraph` -- for methods related to directed graphs.
        * :class:`PartitionGenericGraph` -- for information relating to how the
          partition is stored.
    """
    def __init__(self, data=None, partition=None, check=True, *args, **kwargs):
        """
        When we create this graph, we initialise the partition first, then the
        graph structure. If the graph contains vertices that are not in the
        partition, then these are removed.

        Note that as an empty partition will be treated as a partition with a
        single part with all vertices in it.

        EXAMPLES:

        Create a graph with one partition::

            sage: G = PartitionGraph(graphs.ButterflyGraph())
            sage: G.partition
            defaultdict(<class 'set'>, {0: {0, 1, 2, 3, 4}})

        Remove extraneous vertices::

            sage: G = PartitionGraph(graphs.ButterflyGraph(), partition={0: [0, 3], 1: [1, 2]})
            sage: G.vertices()
            [0, 1, 2, 3]
        """

        # Replace override methods with the orignals to preserve the partition
        PartitionGenericGraph.__init__(self, data, partition, check)
        import types
        self.add_vertex = types.MethodType(DiGraph.add_vertex, self)
        self.add_vertices = types.MethodType(DiGraph.add_vertices, self)
        self.add_edge = types.MethodType(DiGraph.add_edge, self)
        self.add_edges = types.MethodType(DiGraph.add_edges, self)
        DiGraph.__init__(self, data, *args, **kwargs)
        if not self.partition.keys() or not set.union(*self.partition.values()):
            self.partition = defaultdict(set, {0: set(self.vertices())})
        self.add_vertex = types.MethodType(self.__class__.add_vertex, self)
        self.add_vertices = types.MethodType(self.__class__.add_vertices, self)
        self.add_edge = types.MethodType(self.__class__.add_edge, self)
        self.add_edges = types.MethodType(self.__class__.add_edges, self)

        # Remove vertices not in partition (can't be done earlier as we may end
        # up in an infinite chain of recursion)
        if isinstance(data, GenericGraph):
            if self.partition.values() != []:
                vertices = set.union(*self.partition.values())
                cur_vertices = set(data.vertices())
                extra = cur_vertices.difference(vertices)
                if extra:
                    self.partition[0].update(extra)
                    self.subgraph(vertices=vertices, inplace=True)
                    self.partition[0].difference_update(extra)
