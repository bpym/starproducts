# kontsevint

The `kontsevint` Maple file contains the code to calculate the weights of Kontsevich graphs.

## Installation

The `kontsevint` file requires the `HyperInt` Maple package. This can be obtained by running:

~~~~
git submodule init
git submodule update
patch -p1 -d hyperInt < disable_startup_output.patch
~~~~

This `patch` command is necessary as the startup output of hyperInt includes the word ``error''; Sage interprets this as an actual error and raises an exception.
