# Computing Kontsevich integrals

This _Maple_ program computes _Kontsevich weights_, which are integrals of hyperbolic angles over configuration spaces of marked discs, in terms of multiple zeta values. The underlying algorithm is combinatorial and rests mainly on Francis Brown's theory of _hyperlogarithms_ on the moduli space of marked spheres and the idea of _single valued integration_ as promoted by Oliver Schnetz.

## Getting Started

To obtain a copy of this code, either clone the whole repository using
```
git clone https://PanzerErik@bitbucket.org/PanzerErik/kontsevint.git
```
or just download the file [kontsv01.mpl](https://bitbucket.org/PanzerErik/kontsevint/src/master/kontsv01.mpl) (note that this code handles only graphs with `m=2` terrestrial vertices; for more general cases, the file [kontsv.mpl](https://bitbucket.org/PanzerErik/kontsevint/src/master/kontsv.mpl) has to be used instead).

### Prerequisites

The code needs the _HyperInt_ library, which is available [here](https://bitbucket.org/PanzerErik/hyperint).

If everything works, the following *Maple* session
```
read "kontsv01.mpl":
weight([[L,R]]);
```
should run through without any errors and output `1/2`.

## Running the tests

```
maple -q tests.mpl
```

## References

* _in preparation_ Title by Peter Banks, Erik Panzer and Brent Pym
* [Deformation Quantization of Poisson manifolds](https://link.springer.com/article/10.1023/B:MATH.0000027508.00421.bf) - Kontsevich's paper
* [HyperInt](https://arxiv.org/abs/1403.3385) (arXiv), or on [Bitbucket](https://bitbucket.org/PanzerErik/hyperint) - prerequisite

## Author

* **Erik Panzer** [](http://people.maths.ox.ac.uk/panzer/)

## License


## Acknowledgments


