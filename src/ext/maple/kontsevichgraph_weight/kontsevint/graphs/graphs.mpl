# Erik Panzer
# 22 February 2017

# Code to import nauty's digraph6 format


# parse a number, N(n) in the format description
parseN := proc(bytes::evaln(list(integer)))
local S:
	S := eval(bytes):
	if S=[] then
		error "empty list":
	elif S[1]<63 then
		bytes := S[2..]:
		return S[1]:
	elif S[1]<>63 then
		error "expected 63, but got %1", S[1]:
	elif nops(S)=1 then
		error "missing token":
	elif S[2]<63 then
		bytes := S[3..]:
		return S[1]*63+S[2]:
	elif S[2]<>63 then
		error "expected 63, but got %1", S[2]:
	else
		bytes := S[4..]:
		return S[1]*63^2+S[2]*63+S[3]:
	end if:
end proc:

# parse binary string, R(x) in the format description
parseR := proc(bytes::list(integer))
local Pad:
	Pad := (L->[0$(6-nops(L)), op(L)]):
	[seq(op(Pad(ListTools[Reverse](convert(b, 'base', 2)))), b in bytes)]:
end proc:

# parse a digraph6
parseDigraph6 := proc(name::string)
local bytes, n, E, v, w, V:
	if name[1]<>"&" then
		error "first character should be '&'":
	end if:
	bytes := map(c->StringTools[Ord](c)-63, convert(name, 'list')[2..]):
	# number of vertices:
	n := parseN(bytes):
	# adjacency matrix, contains only +1 for outgoing edges
	E := parseR(bytes)[1..n^2]:
	# turn into edge list
	E := [seq(seq(`if`(E[(v-1)*n+w]=1, [v,w], NULL), w=1..n), v=1..n)]:
	# relabel the vertices with outdegree 0 as L and R
	V := {seq(1..n)} minus map2(op, 1, {op(E)}):
	if nops(V)<>2 then
		error "got %1 vertices with outdegree=0", nops(V):
	end if:
	V := min(V), max(V):
	# also relabel the remaining vertices from 1 to n-2
	E := eval(E, [V[1]=L, V[2]=R, seq(v=v-1, v=V[1]+1..V[2]-1), seq(v=v-2, v=V[2]+1..n)]):
	# turn into adjacency list:
	E := [seq(map2(op,2,select(e->e[1]=v, E)), v=1..n-2)]:
end proc:
