from sage.all import *
import sage.env
import os

ss = sage.env.SAGE_SHARE
sage.env.SAGE_SHARE = os.path.dirname(os.path.realpath(__file__))

se = sage.env.SAGE_EXTCODE
sage.env.SAGE_EXTCODE = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'ext')

from graphs import *
from poisson_brackets import *
from star_products import *
from flow_equation import *

sage.env.SAGE_SHARE = ss
sage.env.SAGE_EXTCODE = se
